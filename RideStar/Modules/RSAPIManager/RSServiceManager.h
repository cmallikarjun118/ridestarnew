//
//  RSServiceManager.h
//  RideStar
//
//  Created by Raja Sekhar on 15/05/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

typedef void (^ServerResponseBlock)(BOOL success, NSString* message, NSDictionary *dataDict, NSInteger code);

@interface RSServiceManager : AFHTTPSessionManager

+ (RSServiceManager *)sharedInstance;

-(void)getFromPath:(NSString*)path completion:(ServerResponseBlock)block;

-(void)postToPath:(NSString*)path parameters:(NSDictionary*)params completion:(ServerResponseBlock)block;


@end
