//
//  RSServiceInteractor.m
//  RideStar
//
//  Created by Raja Sekhar on 15/05/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSServiceInteractor.h"

#import "ZMConstant.h"

//kbaseurl: http://54.169.50.112/rideStarApp/?
//mobile=8553070384&password=sudha123&email=&sid=sid56b2ef61827a31.83586481&code=7058&name=sudha
//http://54.169.50.112/rideStarApp/?methodName=favourite.get&user_id=1
// 52.74.142.219/rideStarApp/?methodName=favourite.check&user_id=1&address=test2%20address
//52.74.142.219//rideStarApp/?methodName=online.deals&city=bangalore
//52.74.142.219/rideStarApp/?methodName=bearerToken.get&user_id=12
//52.74.142.219//rideStarApp/?user_id=12&service_name=OLA&token_value=787613cc33dc434dac6af2f787644218

#define USERSIGNIN_PATH   @"methodName=user.signin&type=1&"
#define OTP_PATH          @"methodName=smsOtp.push&phoneNumber="
#define REGISTER_PATH     @"methodName=user.register&"
#define forgotPwdAPI      @"methodName=forgot.password&"
#define getUserDetailsAPI @"methodName=user.action&"
#define GETPROMO          @"methodName=send.promoCode&"

#define FAVplacesDeleteAPI @"methodName=favourite.delete&"
#define FAVplacesCheckAPI @"methodName=favourite.check&"
#define FAVPlacesGetAPI   @"methodName=favourite.get&"
#define FAVplaceAddAPI    @"methodName=favourite.add&"

#define GET_CABSDATA_API  @"methodName=cabs.info&"
#define BOOK_CAB_API      @"methodName=book.cab&"
#define TRACK_CAB_API     @"methodName=ride.tracking&"
#define GETMYRIDESAPI     @"methodName=booking.get&"
#define addMyRideAPI      @"methodName=booking.add&"

#define GET_OffersAPI     @"methodName=online.deals&"


#define CHECKFORTOKEN_API @"methodName=bearerToken.get&"
#define bearerTokenAdd    @"methodName=bearerToken.add&"


@implementation RSServiceInteractor

+(void)signApiCallWith:(NSString *)path withCompetion:(ServerResponseBlock)block{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@",kBaseUrl,USERSIGNIN_PATH,path];
    [[RSServiceManager sharedInstance] getFromPath:urlPath completion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger responsecode) {
        if (block) {
            block(success,message,dataDict,responsecode);
        }
    }];
}

+(void)OTPApiCallWith:(NSString *)path withCompetion:(ServerResponseBlock)block{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@", kBaseUrl,OTP_PATH,path];
    [[RSServiceManager sharedInstance]getFromPath:urlPath completion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (block) {
            block(success, message, dataDict, code);
        }
    }];
}
+(void)REGISTERAPIWith:(NSString *)path withCompetion:(ServerResponseBlock)block{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@",kBaseUrl,REGISTER_PATH,path];
    [[RSServiceManager sharedInstance]getFromPath:urlPath completion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (block) {
            block(success,message,dataDict,code);
        }
    }];
}
+(void)forgotPasswordAPI:(NSString *)path withCompetion:(ServerResponseBlock)block;{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@",kBaseUrl,forgotPwdAPI,path];
    [[RSServiceManager sharedInstance]getFromPath:urlPath completion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (block) {
            block(success,message,dataDict,code);
        }
    }];
    
}

+(void)userAction:(NSString *)path withCompetion:(ServerResponseBlock)block{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@",kBaseUrl,getUserDetailsAPI,path];
    [[RSServiceManager sharedInstance]getFromPath:urlPath completion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (block) {
            block(success,message,dataDict,code);
        }
    }];
    
}

#pragma favourite places request

+(void)favouriteGet:(NSString *)path withCompetion:(ServerResponseBlock)block{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@",kBaseUrl,FAVPlacesGetAPI,path];
    [[RSServiceManager sharedInstance]getFromPath:urlPath completion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (block) {
            block(success, message ,dataDict, code);
        }
    }];
    
}
+(void)favouriteCheck:(NSString *)path withCompetion:(ServerResponseBlock)block{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@",kBaseUrl,FAVplacesCheckAPI,path];
    [[RSServiceManager sharedInstance]getFromPath:urlPath completion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (block) {
            block(success,message,dataDict,code);
        }
    }];
    
}
+(void)favouriteAdd:(NSString *)path withCompetion:(ServerResponseBlock)block{
    NSString *urlpath = [NSString stringWithFormat:@"%@%@%@",kBaseUrl,FAVplaceAddAPI,path];
    [[RSServiceManager sharedInstance]getFromPath:urlpath completion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (block) {
            block(success,message,dataDict,code);
        }
    }];
}
+(void)favouriteDelete:(NSString *)path withCompetion:(ServerResponseBlock)block{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@",kBaseUrl,FAVplacesDeleteAPI,path];
    [[RSServiceManager sharedInstance]getFromPath:urlPath completion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (block) {
            block(success,message,dataDict,code);
        }
    }];
}

#pragma getPromoCode
+(void)GetPromoCode:(NSString *)path withCompletion:(ServerResponseBlock)block{
    NSString *url = [NSString stringWithFormat:@"%@%@%@",kBaseUrl,GETPROMO,path];
    [[RSServiceManager sharedInstance]getFromPath:url completion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (block) {
            block(success,message,dataDict,code);
        }
    }];
}


#pragma mark - GetCabsAPI

+(void)getCabs:(NSString *)path withCompletion:(ServerResponseBlock)block{
    NSString *urlpath = [NSString stringWithFormat:@"%@%@%@",kBaseUrl,GET_CABSDATA_API,path];
    [[RSServiceManager sharedInstance]getFromPath:urlpath completion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (block) {
            block(success,message,dataDict,code);
        }
    }];

}
#pragma DealsAPI
+(void)GetDeals:(NSString *)path withCompetion:(ServerResponseBlock)block{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@",kBaseUrl,GET_OffersAPI,path];
    [[RSServiceManager sharedInstance]getFromPath:urlPath completion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (block) {
            block(success, message,dataDict,code);
        }
    }];
}

#pragma mark - CabBookingAPIs
+(void)bookCab:(NSString *)path withCompletion:(ServerResponseBlock)block{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@",kBaseUrl,BOOK_CAB_API,path];
    [[RSServiceManager sharedInstance]getFromPath:urlPath completion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (block) {
            block(success, message,dataDict,code);
        }
    }];

}

+(void)trackCabDetails:(NSString *)path withCompletion:(ServerResponseBlock)block{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@",kBaseUrl,TRACK_CAB_API,path];
    [[RSServiceManager sharedInstance]getFromPath:urlPath completion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (block) {
            block(success, message,dataDict,code);
        }
    }];
}

+(void)checkForToken:(NSString *)path withCompletion:(ServerResponseBlock)block{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@",kBaseUrl,CHECKFORTOKEN_API,path];
    [[RSServiceManager sharedInstance]getFromPath:urlPath completion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (block) {
            block(success, message,dataDict,code);
        }
    }];
}

+(void)addBearerToken:(NSString *)path withCompletion:(ServerResponseBlock)block{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@",kBaseUrl,bearerTokenAdd,path];
    [[RSServiceManager sharedInstance]getFromPath:urlPath completion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (block) {
            block(success,message,dataDict,code);
        }
    }];
}

#pragma MyRides API
+(void)getMyRides:(NSString *)path withCompletion:(ServerResponseBlock)block{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@",kBaseUrl,GETMYRIDESAPI,path];
    [[RSServiceManager sharedInstance]getFromPath:urlPath completion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (block) {
            block(success, message,dataDict,code);
        }
    }];
}

+(void)addMyRides:(NSString *)path withCompletion:(ServerResponseBlock)block{
    NSString *urlPath = [NSString stringWithFormat:@"%@%@%@",kBaseUrl,addMyRideAPI,path];
    [[RSServiceManager sharedInstance]getFromPath:urlPath completion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (block) {
            block(success,message,dataDict,code);
        }
    }];
}

@end
