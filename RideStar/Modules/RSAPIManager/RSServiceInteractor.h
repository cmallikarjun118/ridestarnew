//
//  RSServiceInteractor.h
//  RideStar
//
//  Created by Raja Sekhar on 15/05/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "RSServiceManager.h"


@interface RSServiceInteractor : NSObject

+(void)signApiCallWith:(NSString *)path withCompetion:(ServerResponseBlock)block;
+(void)OTPApiCallWith:(NSString *)path withCompetion:(ServerResponseBlock)block;
+(void)REGISTERAPIWith:(NSString *)path withCompetion:(ServerResponseBlock)block;
+(void)forgotPasswordAPI:(NSString *)path withCompetion:(ServerResponseBlock)block;
+(void)userAction:(NSString *)path withCompetion:(ServerResponseBlock)block;
+(void)GetPromoCode:(NSString *)path withCompletion:(ServerResponseBlock)block;

+(void)favouriteGet:(NSString *)path withCompetion:(ServerResponseBlock)block;
+(void)favouriteCheck:(NSString *)path withCompetion:(ServerResponseBlock)block;
+(void)favouriteAdd:(NSString *)path withCompetion:(ServerResponseBlock)block;
+(void)favouriteDelete:(NSString *)path withCompetion:(ServerResponseBlock)block;


+(void)getCabs:(NSString *)path withCompletion:(ServerResponseBlock)block;

+(void)GetDeals:(NSString *)path withCompetion:(ServerResponseBlock)block;

+(void)bookCab:(NSString *)path withCompletion:(ServerResponseBlock)block;
+(void)trackCabDetails:(NSString *)path withCompletion:(ServerResponseBlock)block;
+(void)checkForToken:(NSString *)path withCompletion:(ServerResponseBlock)block;
+(void)addBearerToken:(NSString *)path withCompletion:(ServerResponseBlock)block;

+(void)getMyRides:(NSString *)path withCompletion:(ServerResponseBlock)block;
+(void)addMyRides:(NSString *)path withCompletion:(ServerResponseBlock)block;

@end
