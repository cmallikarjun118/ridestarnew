//
//  RSServiceManager.m
//  RideStar
//
//  Created by Raja Sekhar on 15/05/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSServiceManager.h"
#import "ZMConstant.h"

@implementation RSServiceManager


+ (RSServiceManager *)sharedInstance
{
    //  Static local predicate must be initialized to 0
    static RSServiceManager *sharedInstance = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[RSServiceManager alloc] initWithBaseURL:[NSURL URLWithString:kBaseUrl]];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}


- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers|NSJSONReadingAllowFragments];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        //TODO: Remove when release mode
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        [policy setValidatesDomainName:NO];
        [policy setAllowInvalidCertificates:YES];
        self.securityPolicy = policy;
        [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    }
    return self;
}


-(void)getFromPath:(NSString*)path completion:(ServerResponseBlock)block{
    [self GET:path parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self processResponse:responseObject completion:block];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (block) {
            block(NO, error.description, nil, 0);
        }
    }];
}

-(void)postToPath:(NSString*)path parameters:(NSDictionary*)params completion:(ServerResponseBlock)block{
    NSURLSessionDataTask *task =  [self POST:path parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self processResponse:responseObject completion:block];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (block) {
            block(NO, error.description, nil, 0);
        }
        
    }];
    [task setPriority:NSURLSessionTaskPriorityHigh];
}


-(void)processResponse:(id)responseObject completion:(ServerResponseBlock)block{
    NSString *message = nil;
    BOOL success = NO;
    NSDictionary *dataDict = nil;
    NSInteger responsecode = 0;
    if (!responseObject) {
        NSLog(@"Returned no data");
    } else  {
        if ([responseObject isKindOfClass:[NSArray class]]) {
            block(NO,@"No results found",nil,0);
            return;
        }
        NSDictionary *contentDict = (NSDictionary*)responseObject;
        responsecode = [[contentDict objectForKey:@"responseCode"] integerValue];
        if (!contentDict) {
             NSLog(@"Parsing json failed");
            message = @"Invalid Response";
        } else {
             NSLog(@"Response = %@", contentDict);
            success = (responsecode == 001);
        }
        message = [contentDict valueForKey:@"responseInfo"];
        dataDict = [contentDict objectForKey:@"responseMsg"];
    }
    if (block) {
        block(success, message, dataDict, responsecode);
    }
    
}

-(void)cancel{
    @synchronized(self){
        
    }
}

-(instancetype)init{
    if (self= [super init]) {
    }
    return self;
}


@end
