//
//  RSServicesManager.h
//  RideStar
//
//  Created by Local user on 17/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

typedef void (^ServerResponseBlock)(BOOL success, NSString* message, NSDictionary *dataDict, NSInteger code);

@interface RSServicesManager : NSObject

@property (nonatomic , strong) AFHTTPSessionManager *sessionManager;
+ (RSServicesManager *)sharedInstance;
-(void)getFromPath:(NSString*)path completion:(ServerResponseBlock)block;
-(void)postToPath:(NSString*)path parameters:(NSDictionary*)params completion:(ServerResponseBlock)block;
@end
