//
//  RSTopLocationsViewController.m
//  RideStar
//
//  Created by Raja Sekhar on 02/05/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSTopLocationsViewController.h"

@interface RSTopLocationsViewController ()

@end

@implementation RSTopLocationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"TopLocationCellIdentifier";
    RSTopLocationTableViewCell *cell = (RSTopLocationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.placeLabel.text = @"place";
    cell.distanceLabel.text = @"distance";
    cell.discountLabel.text = @"discount";
    return cell;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
