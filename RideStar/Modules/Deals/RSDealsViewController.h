//
//  RSDealsViewController.h
//  RideStar
//
//  Created by Raja Sekhar on 02/05/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPSPageMenu.h"
#import "RSBestDealsViewController.h"
#import "RSTopLocationsViewController.h"
#import "RSCategoriesViewController.h"

@interface RSDealsViewController : UIViewController

@end
