//
//  RSTopLocationTableViewCell.h
//  RideStar
//
//  Created by Raja Sekhar on 02/05/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSTopLocationTableViewCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UILabel *placeLabel,*distanceLabel,*discountLabel;
@property(nonatomic,weak) IBOutlet UIButton *checkedButton;
-(IBAction)checkedButtonAction:(id)sender;

@end
