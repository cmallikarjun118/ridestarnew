//
//  BestDealsCollectionViewCell.h
//  RideStar
//
//  Created by Raja Sekhar on 02/05/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BestDealsCollectionViewCell : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UILabel *label;
@property (nonatomic,weak) IBOutlet UIButton *button;

-(IBAction)buttonAction:(id)sender;

@end
