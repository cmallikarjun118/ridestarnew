//
//  RSBestDealsViewController.h
//  RideStar
//
//  Created by Raja Sekhar on 02/05/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BestDealsCollectionViewCell.h"

@interface RSBestDealsViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>

@property (nonatomic,weak) IBOutlet UICollectionView *collectionView;

@end
