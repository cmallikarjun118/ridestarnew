//
//  RSTopLocationsViewController.h
//  RideStar
//
//  Created by Raja Sekhar on 02/05/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSTopLocationTableViewCell.h"

@interface RSTopLocationsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,weak) IBOutlet UITableView *tableView;

@end
