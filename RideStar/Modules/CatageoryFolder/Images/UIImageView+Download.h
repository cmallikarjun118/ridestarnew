//
//  UIImageView+Download.h
//  Zigy
//
//  Created by Yogesh Veeraraj on 29/02/16.
//  Copyright © 2016 Yogesh Veeraraj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface UIImageView (Download)

-(void)setImageUrlWithAnimation:(NSURL*)url withPlaceholder:(UIImage*)placeHolderImage;

@end
