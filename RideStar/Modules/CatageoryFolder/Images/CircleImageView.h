//  Zigy
//
//  Created by Yogesh Veeraraj on 04/02/16.
//  Copyright © 2016 Yogesh Veeraraj. All rights reserved.

#import <UIKit/UIKit.h>

/**
 * CircleImageView is a UIImageView subclass that renders an image inside a
 * circle that has a drop shadow. It should be given equal width and height.
 *
 */
IB_DESIGNABLE
@interface CircleImageView : UIImageView
@end
