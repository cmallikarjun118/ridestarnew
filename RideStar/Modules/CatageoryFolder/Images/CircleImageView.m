//  Zigy
//
//  Created by Yogesh Veeraraj on 04/02/16.
//  Copyright © 2016 Yogesh Veeraraj. All rights reserved.
//

#import "CircleImageView.h"
#import "UIColor+Zigy.h"

@implementation CircleImageView

- (instancetype)initWithFrame:(CGRect)frame {
  if ((self = [super initWithFrame:frame])) {
    [self sharedInit];
  }
  return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  if ((self = [super initWithCoder:aDecoder])) {
    [self sharedInit];
  }
  return self;
}

- (void)sharedInit {
  self.backgroundColor = [UIColor clearColor];
    self.clipsToBounds = YES;
  CALayer *layer = [super layer];
  layer.shadowOffset = CGSizeMake(0, 1);
  layer.shadowOpacity = 0.25;
  layer.shadowColor = [[UIColor grayColor] CGColor];
  layer.shadowRadius = 4.0;
  layer.borderColor = [[UIColor blackColor] CGColor];
  layer.borderWidth = 1;
}

- (void)layoutSubviews {
  [super layoutSubviews];

  CGSize size = self.bounds.size;
  CGFloat dim = MAX(size.width, size.height);
  self.layer.cornerRadius = dim / 2;
}

- (void)setContentMode:(UIViewContentMode)contentMode {
  // ignore
}

@end
