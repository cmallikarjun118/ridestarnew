//
//  UIImageView+Download.m
//  Zigy
//
//  Created by Yogesh Veeraraj on 29/02/16.
//  Copyright © 2016 Yogesh Veeraraj. All rights reserved.
//

#import "UIImageView+Download.h"


@implementation UIImageView (Download)

-(void)setImageUrlWithAnimation:(NSURL*)url withPlaceholder:(UIImage*)placeHolderImage{
    if (!placeHolderImage) {
        placeHolderImage = [UIImage imageNamed:@"placeholder"];
    }
    [self sd_setImageWithURL:url placeholderImage:placeHolderImage
                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                       if (cacheType == SDImageCacheTypeNone) {
                           self.alpha = 0;
                           [UIView animateWithDuration:0.3 animations:^{
                               self.alpha = 1;
                           }];
                       } else {
                           self.alpha = 1;
                       }
    }];
}


@end
