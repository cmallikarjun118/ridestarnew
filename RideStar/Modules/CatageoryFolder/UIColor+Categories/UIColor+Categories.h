//
//  UIColor+Categories.h
//  UIColor+Categories
//
//  Created by Yogesh Veeraraj on 04/02/16.
//  Copyright © 2016 Yogesh Veeraraj. All rights reserved.
//

#import "UIColor+Macros.h"
#import "UIColor+Integers.h"
