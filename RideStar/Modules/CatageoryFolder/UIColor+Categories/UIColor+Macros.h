//
//  UIColor+Macros.h
//  UIColor+Categories
//
//  Created by Yogesh Veeraraj on 04/02/16.
//  Copyright © 2016 Yogesh Veeraraj. All rights reserved.
//

@import UIKit;


#define COLOR(NAME, OBJECT) + (instancetype)NAME {\
    static UIColor *_NAME;\
    static dispatch_once_t onceToken;\
    dispatch_once(&onceToken, ^{\
        _NAME = OBJECT;\
    });\
    return _NAME;\
}\
