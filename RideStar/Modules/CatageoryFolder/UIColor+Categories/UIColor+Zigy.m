//
//  UIColor+Zigy.m
//  Zigy
//
//  Created by Yogesh Veeraraj on 22/02/16.
//  Copyright © 2016 Yogesh Veeraraj. All rights reserved.
//

#import "UIColor+Zigy.h"

@implementation UIColor (Zigy)

//Main Menu Colors
COLOR(RSYellowColour, [UIColor colorWithIntegerRed:255 green:214 blue:52]);
COLOR(RSBlueColour, [UIColor colorWithIntegerRed:65 green:204 blue:224]);
COLOR(RSLigtGrayColour, [UIColor colorWithIntegerRed:248 green:248 blue:248]);
COLOR(RSDarkGrayColour, [UIColor colorWithIntegerRed:230 green:230 blue:230 ]);

//New Colours
COLOR(RSPeachColour, [UIColor colorWithIntegerRed:255 green:214 blue:189]);
COLOR(RSOrangeColour, [UIColor colorWithIntegerRed:255 green:114 blue:0]);
COLOR(RSBackgroundColour, [UIColor colorWithIntegerRed:229 green:230 blue:231]);
@end
