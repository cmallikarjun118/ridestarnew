//
//  UIViewController+RScommonSettings.m
//  RideStar
//
//  Created by sanjay on 15/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "UIViewController+RScommonSettings.h"
#import "SWRevealViewController.h"
#import "UIColor+Zigy.h"

@implementation UIViewController (RScommonSettings)

-(void)navigationBarSetting{
 
    SWRevealViewController *revealViewController=self.revealViewController;
     UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc]init];
    if (revealViewController) {
        leftBarButton=[[UIBarButtonItem alloc] init];
        [leftBarButton setTarget:self.revealViewController];
        [leftBarButton setImage:[UIImage imageNamed:@"Menu"]];
        [leftBarButton setAction:@selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    self.navigationItem.leftBarButtonItems = @[leftBarButton];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor = [UIColor RSPeachColour];
    self.navigationController.navigationBar.translucent = NO;

}


@end
