//
//  UIViewController+RScommonSettings.h
//  RideStar
//
//  Created by sanjay on 15/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (RScommonSettings)

-(void)navigationBarSetting;

@end
