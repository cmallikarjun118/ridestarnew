//
//  UIColor+Integers.m
//  UIColor+Categories
//
//  Created by Yogesh Veeraraj on 04/02/16.
//  Copyright © 2016 Yogesh Veeraraj. All rights reserved.
//

#import "UIColor+Integers.h"


static const CGFloat redDivisor = 255;
static const CGFloat greenDivisor = 255;
static const CGFloat blueDivisor = 255;

static const CGFloat hueDivisor = 360;
static const CGFloat saturationDivisor = 100;
static const CGFloat brightnessDivisor = 100;

static const CGFloat whiteDivisor = 100;
static const CGFloat alphaDivisor = 100;


@implementation UIColor (Integers)

#pragma mark - Grayscale

+ (instancetype)colorWithIntegerWhite:(NSUInteger)white
{
    return [self colorWithIntegerWhite:white
                                 alpha:alphaDivisor];
}

+ (instancetype)colorWithIntegerWhite:(NSUInteger)white alpha:(NSUInteger)alpha
{
    return [self colorWithWhite:white/whiteDivisor
                          alpha:alpha/alphaDivisor];
}


#pragma mark - RGB

+ (instancetype)colorWithIntegerRed:(NSUInteger)red green:(NSUInteger)green blue:(NSUInteger)blue
{
    return [self colorWithIntegerRed:red
                               green:green
                                blue:blue
                               alpha:alphaDivisor];
}

+ (instancetype)colorWithIntegerRed:(NSUInteger)red green:(NSUInteger)green blue:(NSUInteger)blue alpha:(NSUInteger)alpha
{
    return [self colorWithRed:red/redDivisor
                        green:green/greenDivisor
                         blue:blue/blueDivisor
                        alpha:alpha/alphaDivisor];
}


#pragma mark - HSB

+ (instancetype)colorWithIntegerHue:(NSUInteger)hue saturation:(NSUInteger)saturation brightness:(NSUInteger)brightness
{
    return [self colorWithIntegerHue:hue
                          saturation:saturation
                          brightness:brightness
                               alpha:alphaDivisor];
}

+ (instancetype)colorWithIntegerHue:(NSUInteger)hue saturation:(NSUInteger)saturation brightness:(NSUInteger)brightness alpha:(NSUInteger)alpha
{
    return [self colorWithHue:hue/hueDivisor
                   saturation:saturation/saturationDivisor
                   brightness:brightness/brightnessDivisor
                        alpha:alpha/alphaDivisor];
}

@end
