//
//  UIColor+Zigy.h
//  Zigy
//
//  Created by Yogesh Veeraraj on 22/02/16.
//  Copyright © 2016 Yogesh Veeraraj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+Categories.h"

@interface UIColor (Zigy)

//Main  colours
+(instancetype)RSYellowColour;
+(instancetype)RSBlueColour;
+(instancetype)RSLigtGrayColour;
+(instancetype)RSDarkGrayColour;

//New Colours
+(instancetype)RSPeachColour;
+(instancetype)RSOrangeColour;
+(instancetype)RSBackgroundColour;

@end
