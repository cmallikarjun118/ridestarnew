//
//  RSFavoratePlacesViewController.m
//  RideStar
//
//  Created by Raja Sekhar on 10/04/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSFavoratePlacesViewController.h"


#import "RSServiceInteractor.h"
#import "UIViewController+RScommonSettings.h"

#import "RNActivityView.h"
#import "UIView+RNActivityView.h"

#import "UIColor+Zigy.h"
#import "UIImage+Color.h"
#import "UIScrollView+EmptyDataSet.h"


@interface RSFavoratePlacesViewController ()<DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@end

@implementation RSFavoratePlacesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self navigationBarSetting];
    [self getFavAddress];
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.favouriteAdress.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RSFavoratePlacesCell *cell = (RSFavoratePlacesCell *)[tableView dequeueReusableCellWithIdentifier:@"RSFavoratePlacesCell"];
    NSLog(@"dlakjfah:%@",_favouriteAdress);
    _favDictionary=[self.favouriteAdress objectAtIndex:indexPath.row];
  
    cell.placeName.text = [_favDictionary valueForKey:@"title"];
    cell.fullAdress.text = [_favDictionary valueForKey:@"full_address"];
    return cell;
}


#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dict = [self.favouriteAdress objectAtIndex:indexPath.row];
    if ([self.delegate respondsToSelector:@selector(selectedFavPlace:isSourceLocation:)]) {
        [self.delegate selectedFavPlace:dict isSourceLocation:self.isSourceLocation];
    }
    [self.navigationController popViewControllerAnimated:YES];
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *userId=[defaults objectForKey:@"userId"];
        NSString *urlString = [NSString stringWithFormat:@"user_id=%@&favourite_id=%@",userId,[_favDictionary valueForKey:@"favorite_data_id"]];
        NSLog(@"Url:%@",urlString);
        [RSServiceInteractor favouriteDelete:urlString withCompetion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
            NSLog(@"response: %@",dataDict);
        }];
        [self getFavAddress];
       
    }
}

-(void)getFavAddress{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userId=[defaults objectForKey:@"userId"];
    NSString *urlString = [NSString stringWithFormat:@"user_id=%@", userId];
    NSLog(@"%@",urlString);
    [self.view showCustomActivityView];
    [RSServiceInteractor favouriteGet:urlString withCompetion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        self.favouriteAdress = [dataDict objectForKey:@"data"];
        [self.tableView reloadData];
        [self.view hideActivityView];
    }];
}


#pragma mark - empty DataSource

-(BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView{
    return YES;
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView{
    return [UIImage imageNamed:@"Fav"];
}
- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"No Favorate Places Added";
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:12],
                                 NSForegroundColorAttributeName: [UIColor blackColor],
                                 NSParagraphStyleAttributeName: paragraph};
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView{
    return 50;
}
- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView{
    return -50;
}


- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state
{
    NSString *text = @"ADD FAVORATE PLACES";
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:12],
                                 NSForegroundColorAttributeName: [UIColor whiteColor ],
                                 NSParagraphStyleAttributeName: paragraph};
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}


-(UIImage*)buttonBackgroundImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state{
    return [UIImage imageWithColor:[UIColor RSOrangeColour] size:CGSizeMake(1, 35)];
}


- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
