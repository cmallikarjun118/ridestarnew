//
//  RSFavoratePlacesViewController.h
//  RideStar
//
//  Created by Raja Sekhar on 10/04/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSFavoratePlacesCell.h"

#import "UIColor+Zigy.h"
#import "MBProgressHUD.h"

@protocol favPlacesDelegate <NSObject>

-(void)selectedFavPlace:(NSDictionary *)dict isSourceLocation:(BOOL)sourceLocation;

@end

@interface RSFavoratePlacesViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic , weak) IBOutlet UITableView *tableView;
@property (nonatomic , strong) NSArray *favouriteAdress;
@property (nonatomic, strong) NSDictionary *favDictionary;
@property (nonatomic,strong)MBProgressHUD *progressView;
@property (nonatomic) BOOL isSourceLocation;
@property (nonatomic , weak) id <favPlacesDelegate> delegate;


@end
