//
//  RSFavoratePlacesCell.h
//  RideStar
//
//  Created by Raja Sekhar on 10/04/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface RSFavoratePlacesCell : UITableViewCell 
@property (weak, nonatomic) IBOutlet UILabel *fullAdress;
@property (weak, nonatomic) IBOutlet UILabel *placeName;


@end
