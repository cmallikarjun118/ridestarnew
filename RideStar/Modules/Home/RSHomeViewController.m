//
//  RSHomeViewController.m
//  RideStar
//
//  Created by Raja Sekhar on 05/04/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <HNKGooglePlacesAutocomplete/HNKGooglePlacesAutocomplete.h>

#import "RSHomeViewController.h"
#import "RSFavoratePlacesViewController.h"
#import "RSServiceInteractor.h"

#import "RSCabDetails.h"
#import "RSAutoPlacesCell.h"

#import "RSHomeModel.h"

#import "UIView+RNActivityView.h"
#import "CLPlacemark+HNKAdditions.h"
#import "UIColor+Zigy.h"
#import "UIViewController+RScommonSettings.h"

#import "TSMessage.h"
#import "RNActivityView.h"
#import "SWRevealViewController.h"
#import "FCCurrentLocationGeocoder.h"
#import "FCIPAddressGeocoder.h"

@interface RSHomeViewController ()<favPlacesDelegate>{
    BOOL isSourceLocation;
    BOOL isDestinationLocation;
}

@property(nonatomic,strong) UIBarButtonItem *leftBarButton;
@property (strong, nonatomic) HNKGooglePlacesAutocompleteQuery *searchQuery;
@property (strong, nonatomic) NSArray *searchResults;


@end

@implementation RSHomeViewController{
    CLLocationManager *locationManager;
    CLLocation *myLocation;
    GMSMarker *marker;
    GMSProjection *coordinateForPoint;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self navigationBarSetting];
    [self getCurrentAddress];
    self.title = [NSString stringWithFormat:@"Welcome %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"name"]];
//    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"name"]stringValue].length>0) {
//        self.title= [ NSString stringWithFormat:@"Welcome %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"name"]];
//    }else{
//        self.title= [ NSString stringWithFormat:@"Welcome Guest"];
//    }
    self.tableView.hidden=YES;
    [self isSourceLocation:YES];
    [self.navigationController.view.rn_activityView setupDefaultValues];
    
    CGFloat currentLattitude= [[RSHomeModel sharedInstance].sourceLattitue floatValue];
    CGFloat currentLongitude=[[RSHomeModel sharedInstance].sourceLongitude floatValue];
    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentLattitude longitude: currentLongitude zoom:17];
    [_googleMapView setCamera:camera];
    
    _googleMapView.settings.myLocationButton = YES;
    _googleMapView.settings.indoorPicker = YES;
    
    GMSCameraUpdate *move = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(currentLattitude, currentLongitude) zoom:17];
    [_googleMapView animateWithCameraUpdate:move];
    _googleMapView.delegate=self;
    _googleMapView.settings.zoomGestures = YES;
    _googleMapView.settings.scrollGestures = YES;
    _googleMapView.settings.rotateGestures = YES;
    _googleMapView.settings.tiltGestures = YES;
    _googleMapView.delegate = self;
    // coordinate -33.86,151.20 at zoom level 6.
    _googleMapView.myLocationEnabled = YES;
    marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(currentLattitude, currentLongitude);
    //  marker.title = @"Sydney";
    //marker.snippet = @"Australia";
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = _googleMapView;
    
    
    
    
    /*locationManager = [[CLLocationManager alloc] init];
     FCCurrentLocationGeocoder *geocoder = [FCCurrentLocationGeocoder sharedGeocoder];
     geocoder.canUseIPAddressAsFallback = YES; //(optional, default value is NO. very useful if you need just the approximate user location, such as current country, without asking for permission)
     geocoder.timeFilter = 30;
     geocoder.timeoutErrorDelay = 10;
     [geocoder reverseGeocode:^(BOOL success) {
     if (success) {
     //you can use the shared instance
     [FCCurrentLocationGeocoder sharedGeocoder];
     
     //you can also use as many shared instances as you need
     [FCCurrentLocationGeocoder sharedGeocoderForKey:@"yourKey"];
     
     //or create a new geocoder and set options
     FCCurrentLocationGeocoder *geocoder = [FCCurrentLocationGeocoder new];
     geocoder.canPromptForAuthorization = NO; //(optional, default value is YES)
     geocoder.canUseIPAddressAsFallback = YES; //(optional, default value is NO. very useful if you need just the approximate user location, such as current country, without asking for permission)
     geocoder.timeFilter = 30; //(cache duration, optional, default value is 5 seconds)
     geocoder.timeoutErrorDelay = 10; //(optional, default value is 15 seconds)
     NSLog(@"location:%@",geocoder.location);
     NSString *latitude = [NSString stringWithFormat:@"%.8f",geocoder.location.coordinate.latitude];
     NSLog(@"latitude:%@",latitude);
     NSString *longitude = [NSString stringWithFormat:@"%.8f",geocoder.location.coordinate.longitude];
     NSLog(@"longitude:%@",longitude);
     
     //check if location services are enabled and the current app is authorized or could be authorized
     [geocoder canGeocode]; //returns YES or NO
     
     //current-location forward-geocoding
     [geocoder geocode:^(BOOL success) {
     
     if(success)
     {
     NSLog(@"lattitude:%f", geocoder.location.coordinate.latitude);
     NSLog(@"longitude:%f", geocoder.location.coordinate.longitude);
     //you can access the current location using 'geocoder.location'
     }
     else {
     //you can debug what's going wrong using: 'geocoder.error'
     }
     }];
     
     //current-location reverse-geocoding
     [geocoder reverseGeocode:^(BOOL success) {
     if(success)
     {
     NSLog(@"location:%@",geocoder.location);
     //you can access the current location using 'geocoder.location'
     //you can access the current location placemarks using 'geocoder.locationPlacemarks'
     //you can access the current location first-placemark using 'geocoder.locationPlacemark'
     //you can access the current location country using 'geocoder.locationCountry'
     //you can access the current location country-code using 'geocoder.locationCountryCode'
     //you can access the current location city using 'geocoder.locationCity'
     //you can access the current location zip-code using 'geocoder.locationZipCode'
     //you can access the current location address using 'geocoder.locationAddress'
     }
     else {
     }
     }];
     //check if geocoding
     // [geocoder isGeocoding]; //returns YES or NO
     //cancel geocoding
     // [geocoder cancelGeocode];
     } }];
     */
    self.searchQuery = [HNKGooglePlacesAutocompleteQuery sharedQuery];
    
    // Do any additional setup after loading the view.
}


#pragma Get user current Location
-(void)getCurrentAddress{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager startUpdatingLocation];
}
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    CLLocation *currentLocation = newLocation;
    [RSHomeModel sharedInstance].sourceLongitude = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
    [RSHomeModel sharedInstance].sourceLattitue = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
    
    CLLocationCoordinate2D target =
    CLLocationCoordinate2DMake([[RSHomeModel sharedInstance].sourceLattitue floatValue],[[RSHomeModel sharedInstance].sourceLongitude floatValue]);
    
    [self.googleMapView animateToLocation:target];
    
    if (currentLocation != nil) {
        
        
        [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
            GMSAddress *addressObj = [response firstResult];
            NSArray *locatedAt = addressObj.lines;
            marker.snippet = [locatedAt objectAtIndex:0];
            self.sourceLocationSearchBar.text = [locatedAt objectAtIndex:0];
            
            
        }];
        [locationManager stopUpdatingLocation];
        
        
        
        //    FCCurrentLocationGeocoder *geocoder = [FCCurrentLocationGeocoder sharedGeocoder];
        //
        //    [geocoder canGeocode];
        //    [geocoder reverseGeocode:^(BOOL success) {
        //        NSLog(@"%@, %f,%f",geocoder.locationAddress, geocoder.location.coordinate.latitude,geocoder.location.coordinate.longitude);
        //        self.sourceLocationSearchBar.text = geocoder.locationAddress;
        //        [RSHomeModel sharedInstance].sourceLongitude = [NSString stringWithFormat:@"%f",geocoder.location.coordinate.longitude];
        //        [RSHomeModel sharedInstance].sourceLattitue = [NSString stringWithFormat:@"%f",geocoder.location.coordinate.latitude];
        //
        //    }];
        
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma  mark - ActionMethods

- (IBAction)rideNowAction:(id)sender {
    if (![self.sourceLocationSearchBar.text isEqualToString:@""]) {
        if (![self.sourceLocationSearchBar.text isEqualToString:@""]) {
        RSCabDetails *detailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RSCabDetails"];
        [self.navigationController pushViewController:detailsVC animated:YES];
        }else{
            [self  showErrorMessage:@"Please Enter Destination Location"];
        }
    }else{
        [self  showErrorMessage:@"Please Enter Source Location"];
    }
}

-(IBAction)sourceFavourateButtonAction:(id)sender{
    if (self.sourceLocationSearchBar.text ==nil||[self.sourceLocationSearchBar.text isEqualToString:@""]) {
        [self showFavourite:YES];
    }else{
        [self addToFavourite];
    }
}
-(IBAction)destinationFavourateButtonAction:(id)sender{
    if (self.destinationLocationSearchBar.text ==nil||[self.destinationLocationSearchBar.text isEqualToString:@""]) {
        [self showFavourite:NO];
    }else{
        [self addToFavourite];
    }
    
}
- (IBAction)locateUserPlaceAction:(id)sender {
    [self getCurrentAddress];
}




-(void)showFavourite:(BOOL)sourceLocation{
    RSFavoratePlacesViewController *favPlace=(RSFavoratePlacesViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"RSFavoratePlacesViewController"];
    favPlace.isSourceLocation = sourceLocation;
    favPlace.delegate = self;
    [self.navigationController pushViewController:favPlace animated:YES];
}

-(void)addToFavourite{
    UIAlertController *favouritePlace=   [UIAlertController
                                          alertControllerWithTitle:@"Enter Title"
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             NSString *placeName= [NSString stringWithFormat:@"%@",([favouritePlace.textFields objectAtIndex:0]).text];
                             [self addAddress:placeName];
                             
                             NSLog(@" title Name:%@",placeName);
                         }];
    UIAlertAction *cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action){
                                 [favouritePlace dismissViewControllerAnimated:YES completion:nil];
                             }];
    [favouritePlace addAction:ok];
    [favouritePlace addAction:cancel];
    [favouritePlace addTextFieldWithConfigurationHandler:^(UITextField *favouritePlaceName) {
        favouritePlaceName.keyboardType=UIKeyboardTypeDefault;
        
    }];
    [self presentViewController:favouritePlace animated:YES completion:nil];
}
-(void)addAddress:(NSString *)placeTitle{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userId=[defaults objectForKey:@"userId"];
    NSString *urlString = [NSString stringWithFormat:@"user_id=%@&title=%@&full_address=%@&latitude=%@&longitude=%@",userId,placeTitle,self.sourceLocationSearchBar.text,[RSHomeModel sharedInstance].sourceLattitue ,[RSHomeModel sharedInstance].sourceLongitude];
    NSString *encodedFullUrl = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"encoded url: %@",urlString);
    [RSServiceInteractor favouriteAdd:encodedFullUrl withCompetion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (code==001){
            [self showSuccessMessage:@"Address successfully added to Favourites"];
        }
        else
            NSLog(@"Error");
    }];
    
}
/*
 -(void)addtoFav:(NSString*)placeTitle{
 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
 NSString *userId=[defaults objectForKey:@"userId"];
 NSString *urlString = [NSString stringWithFormat:@"user_id=%@&title=%@&full_address=%@&latitude=%@&longitude=%@",userId,placeTitle,self.sourceLocationSearchBar.text,[RSHomeModel sharedInstance].sourceLattitue ,[RSHomeModel sharedInstance].sourceLongitude];
 NSString *encode = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
 [RSServiceInteractor favouriteCheck:encode withCompetion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
 //NSLog(@"%@",[dataDict valueForKey:@"status"]);
 if ([dataDict valueForKey:@"status"]==0) {
 NSString *urlString = [NSString stringWithFormat:@"user_id=%@&title=%@&full_address=%@&latitude=%@&longitude=%@",userId,placeTitle,self.sourceLocationSearchBar.text,[RSHomeModel sharedInstance].sourceLattitue ,[RSHomeModel sharedInstance].sourceLongitude];
 NSString *encodedFullUrl = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
 
 NSLog(@"encoded url: %@",urlString);
 [RSServiceInteractor favouriteAdd:encodedFullUrl withCompetion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
 if (code==001){
 [self showAlertWithMessage:@"Address successfully added to Favourites"];
 }
 else
 NSLog(@"Error");
 }];
 } else{
 [self showAlertWithMessage:@"Oops! Address already exits"];
 }
 }];
 
 
 }
 */
#pragma mark - UISearchBar Delegate

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    if (searchBar == self.sourceLocationSearchBar ) {
        [self isSourceLocation:YES];
    }else{
        [self isSourceLocation:NO];
    }
    [searchBar setShowsCancelButton:YES animated:YES];
}

-(void)isSourceLocation:(BOOL)sourceLocation{
    isSourceLocation = sourceLocation;
    isDestinationLocation = !sourceLocation;
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if(searchText.length > 0){
        if (searchBar == self.sourceLocationSearchBar ) {
            [self isSourceLocation:YES];
        }else{
            [self isSourceLocation:NO];
        }
        [self.tableView setHidden:NO];
        [self.searchQuery fetchPlacesForSearchQuery: searchText
                                         completion:^(NSArray *places, NSError *error) {
            if (error) {
           NSLog(@"ERROR: %@", error);
          [self handleSearchError:error];
            } else {
         self.searchResults = places;
         [self.tableView reloadData];
        }
            }];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.text = @"";
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    [self.tableView setHidden:YES];
}
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    [self.tableView setHidden:YES];

}
#pragma mark - UITableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.searchResults.count;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RSAutoPlacesCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"RSAutoPlacesCell" forIndexPath:indexPath];
    HNKGooglePlacesAutocompletePlace *thisPlace = self.searchResults[indexPath.row];
    cell.nameLabel.text = thisPlace.name;
    cell.detailLabel.text = thisPlace.description;
    return cell;
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self.sourceLocationSearchBar setShowsCancelButton:NO animated:YES];
    [self.sourceLocationSearchBar resignFirstResponder];
    HNKGooglePlacesAutocompletePlace *selectedPlace = self.searchResults[indexPath.row];
    [CLPlacemark hnk_placemarkFromGooglePlace: selectedPlace
                                       apiKey: self.searchQuery.apiKey
                                   completion:^(CLPlacemark *placemark, NSString *addressString, NSError *error) {
                                       if (placemark) {
                                           CLLocation *location = placemark.location;
                                           CLLocationCoordinate2D coordinate = location.coordinate;
                                           GMSCameraUpdate *move = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude) zoom:17];
                                           [_googleMapView animateWithCameraUpdate:move];
                                           [self.tableView setHidden: YES];
                                           if (isSourceLocation) {
                                               self.sourceLocationSearchBar.text = selectedPlace.name;
                                               [RSHomeModel sharedInstance].sourceLongitude = [NSString stringWithFormat:@"%f",coordinate.longitude];
                                               [RSHomeModel sharedInstance].sourceLattitue = [NSString stringWithFormat:@"%f",coordinate.latitude];
                                               [self.sourceLocationSearchBar setShowsCancelButton:NO animated:YES];
                                               [self.sourceLocationSearchBar resignFirstResponder];
                                           }else{
                                               self.destinationLocationSearchBar.text = selectedPlace.name;
                                               [RSHomeModel sharedInstance].destinationLogitude = [NSString stringWithFormat:@"%f",coordinate.longitude];
                                               [RSHomeModel sharedInstance].destinationLattitude = [NSString stringWithFormat:@"%f",coordinate.latitude];
                                               [self.destinationLocationSearchBar setShowsCancelButton:NO animated:YES];
                                               [self.destinationLocationSearchBar resignFirstResponder];
                                           }
                                       }
                                   }];
}

#pragma mark - Helpers

- (void)addPlacemarkAnnotationToMap:(CLPlacemark *)placemark addressString:(NSString *)address
{
    //    [self.mapView removeAnnotations:self.mapView.annotations];
    //
    //    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    //    annotation.coordinate = placemark.location.coordinate;
    //    annotation.title = address;
    //
    //    [self.mapView addAnnotation:annotation];
}

#pragma googleMaps Marker setting
- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position {
    marker.position = position.target;
    
    
    //    CLGeocoder *geoCoder=[[CLGeocoder alloc]init];
    //
    //    CLLocation *location = [[CLLocation alloc]
    //                            initWithLatitude:marker.position.latitude
    //                            longitude:marker.position.longitude];
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(marker.position.latitude, marker.position.longitude) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
        GMSAddress *addressObj = [response firstResult];
        NSArray *locatedAt = addressObj.lines;
        if (isSourceLocation) {
            self.sourceLocationSearchBar.text = [locatedAt objectAtIndex:0];
            [RSHomeModel sharedInstance].sourceLongitude = [NSString stringWithFormat:@"%f",marker.position.longitude];
            [RSHomeModel sharedInstance].sourceLattitue = [NSString stringWithFormat:@"%f",marker.position.latitude];
        }else{
            self.destinationLocationSearchBar.text = [locatedAt objectAtIndex:0];
            [RSHomeModel sharedInstance].destinationLogitude = [NSString stringWithFormat:@"%f",marker.position.longitude];
            [RSHomeModel sharedInstance].destinationLattitude = [NSString stringWithFormat:@"%f",marker.position.latitude];
        }
        
    }];
  
    
    
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:coordinate completionHandler:^(GMSReverseGeocodeResponse * response, NSError* error) {
        GMSAddress *addressObj = [response firstResult];
        NSArray *locatedAt = addressObj.lines;
        if (isSourceLocation) {
            self.sourceLocationSearchBar.text = [locatedAt objectAtIndex:0];
            [RSHomeModel sharedInstance].sourceLongitude = [NSString stringWithFormat:@"%f",marker.position.longitude];
            [RSHomeModel sharedInstance].sourceLattitue = [NSString stringWithFormat:@"%f",marker.position.latitude];
        }else{
            self.destinationLocationSearchBar.text = [locatedAt objectAtIndex:0];
            [RSHomeModel sharedInstance].destinationLogitude = [NSString stringWithFormat:@"%f",marker.position.longitude];
            [RSHomeModel sharedInstance].destinationLattitude = [NSString stringWithFormat:@"%f",marker.position.latitude];
        }
    }];
}
- (void)recenterMapToPlacemark:(CLPlacemark *)placemark
{
    //    MKCoordinateRegion region;
    //    MKCoordinateSpan span;
    //
    //    span.latitudeDelta = 0.02;
    //    span.longitudeDelta = 0.02;
    //
    //    region.span = span;
    //    region.center = placemark.location.coordinate;
    //
    //    [self.mapView setRegion:region animated:YES];
}


- (void)handleSearchError:(NSError *)error
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:error.localizedDescription
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark  - favPlacesDelegate

-(void)selectedFavPlace:(NSDictionary *)dict isSourceLocation:(BOOL)sourceLocation{
    if (sourceLocation) {
        self.sourceLocationSearchBar.text = [dict objectForKey:@"full_address"];
        [RSHomeModel sharedInstance].sourceLattitue = [dict objectForKey:@"latitude"];
        [RSHomeModel sharedInstance].sourceLongitude = [dict objectForKey:@"longitude"];
    }else{
        self.destinationLocationSearchBar.text = [dict objectForKey:@"full_address"];
        [RSHomeModel sharedInstance].destinationLattitude = [dict objectForKey:@"latitude"];
        [RSHomeModel sharedInstance].destinationLogitude = [dict objectForKey:@"longitude"];
    }
}

#pragma mark - AlertMessages
-(void)showSuccessMessage:(NSString *)tittle{
    [TSMessage showNotificationWithTitle:@"Congratulation" subtitle:tittle type:TSMessageNotificationTypeSuccess];
}

-(void)showErrorMessage:(NSString *)tittle{
    [TSMessage showNotificationWithTitle:@"Oops" subtitle:tittle type:TSMessageNotificationTypeError];
}

@end
