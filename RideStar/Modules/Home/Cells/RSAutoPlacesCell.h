//
//  RSAutoPlacesCell.h
//  RideStar
//
//  Created by Local user on 08/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSAutoPlacesCell : UITableViewCell
@property (nonatomic , weak) IBOutlet UILabel *nameLabel ,*detailLabel;
@end
