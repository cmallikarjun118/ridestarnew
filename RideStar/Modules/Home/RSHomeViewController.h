//
//  RSHomeViewController.h
//  RideStar
//
//  Created by Raja Sekhar on 05/04/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <GoogleMaps/GoogleMaps.h>

#import "MBProgressHUD.h"



@interface RSHomeViewController : UIViewController<UISearchBarDelegate,UISearchControllerDelegate,UISearchDisplayDelegate,GMSMapViewDelegate,UIScrollViewDelegate,CLLocationManagerDelegate,UITableViewDelegate,UITableViewDataSource, CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet GMSMapView *googleMapView;

@property(nonatomic,weak)IBOutlet UIButton *sourceFavourateButton,*destinationFavourateButton,*nowButton,*todayButton,*laterButton;
@property (weak, nonatomic) IBOutlet UISearchBar *sourceLocationSearchBar;
@property (weak, nonatomic) IBOutlet UISearchBar *destinationLocationSearchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

-(IBAction)sourceFavourateButtonAction:(id)sender;
-(IBAction)destinationFavourateButtonAction:(id)sender;

- (IBAction)locateUserPlaceAction:(id)sender;

-(IBAction)rideNowAction:(id)sender;

@end
