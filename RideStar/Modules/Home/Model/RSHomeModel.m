//
//  RSHomeModel.m
//  RideStar
//
//  Created by Local user on 09/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//


#import "RSHomeModel.h"


@implementation RSHomeModel

@synthesize sourceLongitude;
@synthesize destinationLogitude;
@synthesize sourceLattitue;
@synthesize destinationLattitude;
@synthesize  cabType;
@synthesize  cabId;
@synthesize bookedCabDetails;


+(RSHomeModel *)sharedInstance
{
    static RSHomeModel *sharedInstance = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[RSHomeModel alloc] init];
    });
    return sharedInstance;
}

@end
