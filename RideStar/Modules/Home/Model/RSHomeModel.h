//
//  RSHomeModel.h
//  RideStar
//
//  Created by Local user on 09/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSHomeModel : NSObject
+(RSHomeModel *)sharedInstance;
@property (nonatomic , strong) NSString *sourceLattitue,*sourceLongitude,*destinationLogitude , *destinationLattitude;
@property (nonatomic , strong) NSMutableArray *cabsArray, *sharesArray, *autoArray , *shuttleArray;
@property (nonatomic , strong) NSString *cabType;
@property (nonatomic , strong) NSString *cabId;
@property(nonatomic, strong) NSDictionary *bookedCabDetails;
@end
