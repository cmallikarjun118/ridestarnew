//
//  RSBookingHistoryViewController.h
//  RideStar
//
//  Created by Raja Sekhar on 14/04/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

@interface RSBookingHistoryViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>

@property (nonatomic,weak) IBOutlet UITableView *tableView;

@property(nonatomic,strong) NSArray *bookingArrayData;
@end
