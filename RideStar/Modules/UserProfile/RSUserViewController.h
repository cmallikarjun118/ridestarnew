//
//  RSUserViewController.h
//  RideStar
//
//  Created by Raja Sekhar on 14/04/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPSPageMenu.h"
#import "RSBookingHistoryViewController.h"
#import "RSOffersViewController.h"
#import "RSUserProfileViewController.h"
#import "UIColor+Zigy.h"
#import "SWRevealViewController.h"

@interface RSUserViewController : UIViewController

@property (nonatomic) CAPSPageMenu *pageMenu;


@end
