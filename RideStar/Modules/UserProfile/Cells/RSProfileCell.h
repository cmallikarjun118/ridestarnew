//
//  RSProfileCell.h
//  RideStar
//
//  Created by Raja Sekhar on 14/05/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSProfileCell : UITableViewCell
@property (nonatomic , weak) IBOutlet UILabel *typeLabel;
@property (nonatomic , weak) IBOutlet UITextField *textField;



@end
