//
//  RSBookingHistoryCell.h
//  RideStar
//
//  Created by Raja Sekhar on 14/05/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSBookingHistoryCell : UITableViewCell
//@property (nonatomic , weak) IBOutlet UILabel *dateLabel , *addressLabel ,*distanceLabel ;
@property (nonatomic , weak) IBOutlet UIView *holderView;
@property (weak, nonatomic) IBOutlet UIView *priceHolderView;
@property (weak, nonatomic) IBOutlet UILabel *cabProvider;
@property (weak, nonatomic) IBOutlet UILabel *sourceLocation;
@property (weak, nonatomic) IBOutlet UILabel *destinationLocation;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;


-(void)setBookingDetails:(NSDictionary *)details;

@end
