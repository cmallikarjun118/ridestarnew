//
//  RSUserViewController.m
//  RideStar
//
//  Created by Raja Sekhar on 14/04/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSUserViewController.h"

@interface RSUserViewController ()
@property(nonatomic,strong) UIBarButtonItem *leftBarButton;

@end

@implementation RSUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"RideStar";
    SWRevealViewController *revealViewController=self.revealViewController;
    if (revealViewController) {
        self.leftBarButton=[[UIBarButtonItem alloc] init];
        [_leftBarButton setTarget:self.revealViewController];
        [self.leftBarButton setImage:[UIImage imageNamed:@"MenuIcon"]];
        [_leftBarButton setAction:@selector(revealToggle:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    self.navigationItem.leftBarButtonItems = @[self.leftBarButton];
 
    
    
   
    
    
    
    RSUserProfileViewController *controller1 = (RSUserProfileViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"RSUserProfileViewController"];
    controller1.title = @"BASIC INFO";
    RSBookingHistoryViewController *controller2 = (RSBookingHistoryViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"RSBookingHistoryViewController"];
    controller2.title = @"BOOKINGS";
    RSOffersViewController *controller3 = (RSOffersViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"RSOffersViewController"];
    controller3.title = @"OFFERS";
    NSArray *controllerArray = @[controller1, controller2, controller3];
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor RSDarkGrayColour],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionUnselectedMenuItemLabelColor: [UIColor blackColor],
                                 CAPSPageMenuOptionSelectionIndicatorColor: [UIColor RSBlueColour],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor RSDarkGrayColour],
                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"Helvetica" size:14.0],
                                 CAPSPageMenuOptionSelectedMenuItemLabelColor: [UIColor RSBlueColour],
                                 CAPSPageMenuOptionMenuHeight: @(40.0),
                                 CAPSPageMenuOptionMenuItemWidth: @(90.0),
                                 CAPSPageMenuOptionCenterMenuItems: @(YES)
                                 };
    
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 67, self.view.frame.size.width, self.view.frame.size.height) options:parameters];
    [self.view addSubview:_pageMenu.view];    // Do any additional setup after loading the view.
   
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
