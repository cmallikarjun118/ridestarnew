//
//  RSBookingHistoryViewController.m
//  RideStar
//
//  Created by Raja Sekhar on 14/04/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSBookingHistoryViewController.h"
#import "UIViewController+RScommonSettings.h"
#import "RSBookingHistoryCell.h"

#import "UIColor+Zigy.h"
#import "RSServiceInteractor.h"

#import "RNActivityView.h"
#import "UIView+RNActivityView.h"



@interface RSBookingHistoryViewController ()

@end

@implementation RSBookingHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self navigationBarSetting];
    [self getMyrides];
    self.view.backgroundColor = [UIColor RSBackgroundColour];
    self.title = @"My Rides";
    
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    // A little trick for removing the cell separators
    self.tableView.tableFooterView = [UIView new];
}

#pragma mark:UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.bookingArrayData.count ;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *bookingCellIdentifier = @"RSBookingHistoryCell";
    RSBookingHistoryCell *bookingCell = [tableView dequeueReusableCellWithIdentifier:bookingCellIdentifier forIndexPath:indexPath];
    NSDictionary *dict = [self.bookingArrayData objectAtIndex:indexPath.row];
    //[bookingCell setBookingDetails:dict];
    bookingCell.cabProvider.text =[NSString stringWithFormat:@"%@   %@", [dict valueForKey:@"provider"],[dict valueForKey:@"product_type"]];
    bookingCell.sourceLocation.text      = [NSString stringWithFormat:@"From:%@", [dict valueForKey:@"source_location"]];
    bookingCell.sourceLocation.textColor = [UIColor brownColor];
    bookingCell.destinationLocation.text = [NSString stringWithFormat:@"To:  %@",[dict valueForKey:@"destination_location"]];
    bookingCell.destinationLocation.textColor = [UIColor brownColor];
    bookingCell.descriptionLabel.text    = [NSString stringWithFormat:@"₹ %@ | %@ | %@ | %@",[dict valueForKey:@"exact_ride_price"],[dict valueForKey:@"distance"],[dict valueForKey:@"ride_time"],[dict valueForKey:@"cab_booking_info_id"] ];
    
    return bookingCell;
}


#pragma mark:UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getMyrides{
        [self.view showActivityView];
//    NSString *urlPath = [NSString stringWithFormat:@"user_id=1"];
        NSString *urlPath = [NSString stringWithFormat:@"user_id=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userId"]];
    [RSServiceInteractor getMyRides:urlPath withCompletion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        [self.view hideActivityView];
        if (success) {
            
        self.bookingArrayData = [dataDict objectForKey:@"booking_data"];
        [self.tableView reloadData];
        }
    }];
}

#pragma mark - dznemtydataset

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"No current or past bookings found ";
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0f],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}
@end
