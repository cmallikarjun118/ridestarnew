//
//  RSignUpViewController.m
//  RideStar
//
//  Created by Raja Sekhar on 10/04/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSignUpViewController.h"
#import "RSOTPViewController.h"
#import "RSLoginViewController.h"

#import "UIColor+Zigy.h"
#import "RSRegisterCell.h"
#import "ZMConstant.h"
#import "PageUtils.h"
#import "RSServiceInteractor.h"
#import "RSLoginModel.h"

#import "TSMessage.h"
#import "RNActivityView.h"
#import "UIView+RNActivityView.h"


@interface RSignUpViewController ()

{
    NSMutableArray *rigisterArray;

}
@property (nonatomic , strong) NSString *(^getFirstName)(void);
@property(nonatomic,strong) UIBarButtonItem *leftBarButton;

@end
static NSString *RSUserSection = @"Register Section";


static NSString *RSUserName = @"UserName";
static NSString *RSPhoneNumber = @"Phone Number";
static NSString *RSEmailId = @"EMail Id";
static NSString *RSPassword = @"Password";
static NSString *RSConfirmPassWord = @"Confirm Password";
static NSString *RSPromoCode = @" Promo Code";

static NSString *RSPasswordMessage   = @"Enter UserPassword";
static NSString *RSUserNameMessage= @"Enter USerName";
static NSString *RSPhoneNumberMessage = @"Enter PhoneNumber";
static NSString *RSEMailIdMessage = @"Enter EmailID";
static NSString *RSConfirmPassWordMessage = @"Enter Confirm Password";
static NSString *RSPromoCodeMessage = @" Enter Promo Code";

@implementation RSignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self intialSetUp];
    self.title = @"Sign Up";
    // Do any additional setup after loading the view.
}
-(void)intialSetUp{
    self.contents=[[NSMutableDictionary alloc]init];
    rigisterArray=[[NSMutableArray alloc]init];
    [rigisterArray addObject:[NSMutableDictionary dictionaryWithObjects:@[@"",RSUserName,RSUserNameMessage,@YES] forKeys:@[RSValue,RSTittle,RSMessage,RSTextFieldEnabled]]];
    [rigisterArray addObject:[NSMutableDictionary dictionaryWithObjects:@[@"",RSPhoneNumber,RSPhoneNumberMessage,@YES] forKeys:@[RSValue,RSTittle,RSMessage,RSTextFieldEnabled]]];
    [rigisterArray addObject:[NSMutableDictionary dictionaryWithObjects:@[@"",RSEmailId,RSEMailIdMessage,@YES] forKeys:@[RSValue,RSTittle,RSMessage,RSTextFieldEnabled]]];
    [rigisterArray addObject:[NSMutableDictionary dictionaryWithObjects:@[@"",RSPassword,RSPasswordMessage,@YES] forKeys:@[RSValue,RSTittle,RSMessage,RSTextFieldEnabled]]];
    [rigisterArray addObject:[NSMutableDictionary dictionaryWithObjects:@[@"",RSConfirmPassWord,RSConfirmPassWordMessage,@YES] forKeys:@[RSValue,RSTittle,RSMessage,RSTextFieldEnabled]]];
     [rigisterArray addObject:[NSMutableDictionary dictionaryWithObjects:@[@"",RSPromoCode,RSPromoCodeMessage,@YES] forKeys:@[RSValue,RSTittle,RSMessage,RSTextFieldEnabled]]];
    self.sections=@[RSUserSection];
    [self.contents setObject:rigisterArray forKey:RSUserSection];}

#pragma mark - UITableViewDataSource
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.sections.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSString *sectionTitle=[self.sections objectAtIndex:section];
    return [[self.contents objectForKey:sectionTitle] count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *placeHolder=[self getTitleForIndexPath:indexPath];
    static NSString *registerCellIdentifier=@"RSRegisterCell";
    RSRegisterCell *registercel=[tableView dequeueReusableCellWithIdentifier:registerCellIdentifier forIndexPath:indexPath];
    registercel.selectionStyle = UITableViewCellSelectionStyleNone;
    registercel.textField.placeholder=placeHolder;
    registercel.textField.delegate = self;
    if ([placeHolder isEqualToString:RSUserName]){
        registercel.textField.keyboardType = UIKeyboardTypeDefault;
    }
    else if ([placeHolder isEqualToString:RSPhoneNumber]){
        registercel.textField.keyboardType = UIKeyboardTypePhonePad;
    }
    else if ([placeHolder isEqualToString:RSEmailId]){
        registercel.textField.keyboardType = UIKeyboardTypeEmailAddress;
    }
    else if ([placeHolder isEqualToString:RSPassword]){
        registercel.textField.secureTextEntry=YES;
    }
    else if ([placeHolder isEqualToString:RSConfirmPassWord]){
        registercel.textField.text=UIKeyboardTypeDefault;
        registercel.textField.secureTextEntry=YES;
    }
    else if ([placeHolder isEqualToString:RSPromoCode]){
        registercel.textField.text = UIKeyboardTypeDefault;
    }
    return registercel;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
        return 60;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *fotterView=[[UIView alloc] initWithFrame:CGRectMake(0,0, self.tableView.frame.size.width, 50)];
    fotterView.backgroundColor=[UIColor clearColor];
    UIButton *loginButton=[[UIButton alloc] initWithFrame:CGRectMake(0, 20, self.tableView.frame.size.width, 40)];
    loginButton.backgroundColor=[UIColor RSOrangeColour];
    [loginButton addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
    [loginButton setTitle:@"Submit" forState:UIControlStateNormal];
    [loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    loginButton.layer.cornerRadius = 10.0;
    [fotterView addSubview:loginButton];
    return fotterView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - ActionMethods

-(IBAction)cancelButtonAction:(id)sender{
    RSOTPViewController *otpController  = [self.storyboard instantiateViewControllerWithIdentifier:@"RSOTPViewController"];
    [self.navigationController pushViewController:otpController animated:YES];
}

- (IBAction)submitAction:(UIButton *)sender {
    [self resignKeyBoard];
    NSString *phoneNumber;
    NSString *confirmPwd;
    NSString *password;
    NSString *userName;
    NSString * emailId;
    NSString *promo;
    for (int i = 0; i< [[self.contents objectForKey:RSUserSection] count]; i++) {
        NSDictionary *dict = [[self.contents objectForKey:RSUserSection] objectAtIndex:i];
        if ([[dict objectForKey:RSTittle] isEqualToString:RSPhoneNumber]) {
            phoneNumber = [dict objectForKey:RSValue];
            NSLog(@"Phone: %@",phoneNumber);
        }
        if ([[dict objectForKey:RSTittle] isEqualToString:RSPassword]){
            password = [dict objectForKey:RSValue];
        }
        if ([[dict objectForKey:RSTittle]isEqualToString:RSConfirmPassWord]) {
            confirmPwd = [dict objectForKey:RSValue];
        }
        if ([[dict objectForKey:RSTittle]isEqualToString:RSEmailId]) {
            emailId = [dict objectForKey:RSValue];
            
        }
        if ([[dict objectForKey:RSTittle]isEqualToString:RSUserName]) {
            userName = [dict objectForKey:RSValue];
        }
        if ([[dict objectForKey:RSTittle]isEqualToString:RSPromoCode]) {
            promo = [dict objectForKey:RSValue];
        }
    }
    if (userName.length>0) {
        if (phoneNumber.length>0) {
            if ([PageUtils validateMobileNumber:phoneNumber]) {
                if (emailId.length>0) {
                    if ([PageUtils validateEmailWithString:emailId]){
                        if (password.length>0) {
                            if (confirmPwd.length>0) {
                                if ( password==confirmPwd) {
                                    [self.view showCustomActivityView];
                                    NSString *urlString = [NSString stringWithFormat:@"%@",phoneNumber];
                                    [RSServiceInteractor OTPApiCallWith:urlString withCompetion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
                                        [self.view hideActivityView];
                                        if (success) {
                                            RSOTPViewController *otpController  = [self.storyboard instantiateViewControllerWithIdentifier:@"RSOTPViewController"];
                                            [RSLoginModel sharedInstance].mobileNumber = phoneNumber;
                                            [RSLoginModel sharedInstance].userName = userName;
                                            [RSLoginModel sharedInstance].password = password;
                                            [RSLoginModel sharedInstance].email = emailId;
                                            [RSLoginModel sharedInstance].promoCode = promo;
                                            [RSLoginModel sharedInstance].sidValue = [NSString stringWithFormat:@"%@",[dataDict objectForKey:@"sid"]];
                                            [self.navigationController pushViewController:otpController animated:YES];
                                        }
                                    }];
                                }else{
                                     [self showErrorMessage:@"Password Not matching"];
                                }
                            }else{
                                  [self showErrorMessage:@"Please Confirm Password"];
                            }
                        }else{
                            [self showErrorMessage:@"Please Enter Password"];
                        }
                    }else{
                        [self showErrorMessage:@"Please Enter Valid Email Id"];
                    }
                }else{
                    [self showErrorMessage:@"Please Enter Valid Email Id"];
                }
            }else{
                [self showErrorMessage:@"Phone number Should be of 10 digit number"];
            }
        }else{
            [self showErrorMessage:@"Please Enter Phone Number"];
        }
    }else{
        [self showErrorMessage:@"Please Enter User Name"];
    }
}
#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSIndexPath *indexPath = [self indexPathForView:textField];
    NSString *title = [self getTitleForIndexPath:indexPath];
    if ( [title isEqualToString:RSPhoneNumber]){
        return [PageUtils phoneNumberWithRange:range withString:string withlengh:10 withTextField:textField.text];
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (![self.tableView focusNextTextField]) {
        [textField resignFirstResponder];
    }
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSIndexPath *indexPath = [self indexPathForView:textField];
    NSString *type = [self.sections objectAtIndex:indexPath.section];
    id content = [[self.contents objectForKey:type] objectAtIndex:indexPath.row];
    if ([content isKindOfClass:[NSDictionary class]]) {
        [(NSDictionary*)content setValue:textField.text forKey:RSValue];
    }
}

-(void)resignKeyBoard{
    [[self.tableView TPKeyboardAvoiding_findFirstResponderBeneathView:self.view] resignFirstResponder];
}


#pragma mark - healperMethods

-(NSIndexPath*)indexPathForView:(UIView*)view{
    while (view && ![view isKindOfClass:[UITableViewCell class]])
        view = view.superview;
    return [self.tableView indexPathForRowAtPoint:view.center];
}

-(NSString*)getTitleForCurrentTextfield{
    UITextField *textfield =  (UITextField*)[self.tableView TPKeyboardAvoiding_findFirstResponderBeneathView:self.view];
    NSIndexPath *indexPath = [self indexPathForView:textfield];
    return [self getTitleForIndexPath:indexPath];
}
-(NSString*)getTitleForIndexPath:(NSIndexPath*)indexPath
{
    NSString *type = [self.sections objectAtIndex:indexPath.section];
    id content = [[self.contents objectForKey:type] objectAtIndex:indexPath.row];
    if ([content isKindOfClass:[NSDictionary class]]) {
        return [(NSDictionary*)content objectForKey:RSTittle];
    }else{
        return (NSString*)content;
    }
}

-(NSString*)getValueForIndexPath:(NSIndexPath*)indexPath{
    NSString *type = [self.sections objectAtIndex:indexPath.section];
    id content = [[self.contents objectForKey:type] objectAtIndex:indexPath.row];
    if ([content isKindOfClass:[NSDictionary class]]) {
        return [(NSDictionary*)content objectForKey:RSValue];
    }else{
        return (NSString*)content;
    }
}

-(void)showSuccessMessage:(NSString *)tittle{
    [TSMessage showNotificationWithTitle:@"Sign Up" subtitle:tittle type:TSMessageNotificationTypeSuccess];
}

-(void)showErrorMessage:(NSString *)tittle{
    [TSMessage showNotificationWithTitle:@"Sign Up" subtitle:tittle type:TSMessageNotificationTypeError];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
