//
//  RSForgotViewController.m
//  RideStar
//
//  Created by sanjay on 12/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSForgotViewController.h"
#import "RSResetPasswordViewController.h"
#import "RSServiceInteractor.h"
#import "UIColor+Zigy.h"
#import "PageUtils.h"
#import "RSLoginModel.h"

#import "TSMessage.h"
#import "RNActivityView.h"
#import "UIView+RNActivityView.h"

@interface RSForgotViewController ()
@property(nonatomic,strong) UIBarButtonItem *leftBarButton;
@end

@implementation RSForgotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"Forgot Password?";
    self.view.backgroundColor = [UIColor RSBackgroundColour];
    self.cancelButton.layer.cornerRadius = 20.0f;
    self.resetButton.layer.cornerRadius = 20.0f;

    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - HealperMethods

-(void)resetParentView {
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.4 animations:^{
            CGRect frame = self.view.frame;
            frame.origin.y = +50;
            self.view.frame = frame;
        }];
    });
}

-(void) animateparentView {
    [UIView animateWithDuration:0.4 animations:^{
        CGRect frame = self.view.frame;
        frame.origin.y = -50;
        self.view.frame = frame;
    }];
}


- (IBAction)resetButtonAction:(id)sender {
    if (self.phoneNumber.text.length>0) {
        if ([PageUtils validateMobileNumber:self.phoneNumber.text]) {
            [self.view showCustomActivityView];
            NSString *urlString = [NSString stringWithFormat:@"%@",self.phoneNumber.text];
            [RSServiceInteractor OTPApiCallWith:urlString withCompetion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
                [self.view hideActivityView];
                if (success) {
                    NSLog(@"%@",dataDict);
                    NSString *sid = [NSString stringWithFormat:@"%@",[dataDict objectForKey: @"sid"] ];
                    NSLog(@"sid is:%@",sid);
                    RSResetPasswordViewController *reset = [self.storyboard instantiateViewControllerWithIdentifier:@"RSResetPasswordViewController"];
                    [RSLoginModel sharedInstance].sidValue  = [dataDict objectForKey:@"sid"];
                    [RSLoginModel sharedInstance].mobileNumber  = [NSString stringWithFormat:@"%@",self.phoneNumber.text];
                    [self.navigationController pushViewController:reset animated:YES];
                }
            }];
           }
          }
    else{
        [self showErrorMessage:@"Mobile Mandatory"];
    }
}

- (IBAction)cancelButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self animateparentView];
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self resetParentView];
}

#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
        return [PageUtils phoneNumberWithRange:range withString:string withlengh:10 withTextField:textField.text];
}

#pragma mark - AlertMessages
-(void)showSuccessMessage:(NSString *)tittle{
    [TSMessage showNotificationWithTitle:@"Success" subtitle:tittle type:TSMessageNotificationTypeSuccess];
}

-(void)showErrorMessage:(NSString *)tittle{
    [TSMessage showNotificationWithTitle:@"Oops" subtitle:tittle type:TSMessageNotificationTypeError];
}
@end
