//
//  RSLoginCell.m
//  RideStar
//
//  Created by Raja Sekhar on 05/04/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSLoginCell.h"
#import "UIColor+Zigy.h"

@implementation RSLoginCell

- (void)awakeFromNib {
    self.holderView.layer.borderColor =[UIColor RSLigtGrayColour].CGColor;
    self.holderView.layer.borderWidth =2.0;
    
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
