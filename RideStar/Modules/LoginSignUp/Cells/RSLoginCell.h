//
//  RSLoginCell.h
//  RideStar
//
//  Created by Raja Sekhar on 05/04/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSLoginCell : UITableViewCell
@property (nonatomic , weak) IBOutlet UIImageView *image;
@property (nonatomic , weak) IBOutlet UITextField *textField;
@property (nonatomic , weak) IBOutlet UIView *holderView;

@end
