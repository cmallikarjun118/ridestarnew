//
//  RSRegisterCell.h
//  RideStar
//
//  Created by sanjay on 07/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSRegisterCell : UITableViewCell
@property (nonatomic , weak) IBOutlet UITextField *textField;
@end
