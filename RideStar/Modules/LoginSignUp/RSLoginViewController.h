//
//  RSLoginViewController.h
//  RideStar
//
//  Created by Raja Sekhar on 05/04/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingTableView.h"

@interface RSLoginViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIAlertViewDelegate>
@property (nonatomic , weak) IBOutlet TPKeyboardAvoidingTableView *tableView;
@property (nonatomic , strong) NSArray *sections;
@property (nonatomic , strong) NSMutableDictionary *contents;
@property (nonatomic , weak) IBOutlet UIView *holderView;
@end
