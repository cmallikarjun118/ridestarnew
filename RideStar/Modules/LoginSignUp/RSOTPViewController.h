//
//  RSOTPViewController.h
//  RideStar
//
//  Created by Raja Sekhar on 14/05/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RSSharedViewController.h"

@interface RSOTPViewController : RSSharedViewController<UITextFieldDelegate>
@property (nonatomic , weak) IBOutlet UITextField *otpTextField;
@property (nonatomic , weak) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
- (IBAction)cancelBtn:(id)sender;
- (IBAction)submitBtn:(id)sender;





@end
