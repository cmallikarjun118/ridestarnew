//
//  RSForgotViewController.h
//  RideStar
//
//  Created by sanjay on 12/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RSSharedViewController.h"

@interface RSForgotViewController : RSSharedViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *buttonsHolderView;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UITextField *phoneNumber;

- (IBAction)resetButtonAction:(id)sender;
- (IBAction)cancelButtonAction:(id)sender;


@end
