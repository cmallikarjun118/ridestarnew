//
//  RSResetPasswordViewController.h
//  RideStar
//
//  Created by Raja Sekhar on 14/05/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingTableView.h"
#import "RSSharedViewController.h"


@interface RSResetPasswordViewController : RSSharedViewController<UITableViewDataSource , UITableViewDelegate, UITextFieldDelegate>
@property (nonatomic , weak) IBOutlet TPKeyboardAvoidingTableView *tableView;
@property (nonatomic , strong) NSArray *sections;
@property (nonatomic , strong) NSMutableDictionary *contents;
@property (nonatomic , weak) IBOutlet UIView *holderView;
@end
