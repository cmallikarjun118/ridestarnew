//
//  RSLoginModel.m
//  RideStar
//
//  Created by Local user on 18/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSLoginModel.h"

@implementation RSLoginModel
@synthesize userName;
@synthesize password;
@synthesize email;
@synthesize mobileNumber;
@synthesize promoCode;
@synthesize sidValue;
@synthesize userId;

+(RSLoginModel *)sharedInstance{
    static RSLoginModel *sharedInstance = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[RSLoginModel alloc] init];
    });
    return sharedInstance;
}

@end
