//
//  RSLoginModel.h
//  RideStar
//
//  Created by Local user on 18/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSLoginModel : NSObject
+(RSLoginModel *)sharedInstance;
@property (nonatomic , strong) NSString *userName,*password,*mobileNumber,*email ,*sidValue , *promoCode , *userId;
@property (nonatomic) BOOL isOlaToken , isUbertoken;
@end
