//
//  RSLoginViewController.m
//  RideStar
//
//  Created by Raja Sekhar on 05/04/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSLoginViewController.h"
#import "RSignUpViewController.h"
#import "RSHomeViewController.h"
#import "RSForgotViewController.h"
#import "RSLoginCell.h"
#import "SWRevealViewController.h"

#import "RSLoginModel.h"

#import "ZMConstant.h"
#import "UIColor+Zigy.h"
#import "PageUtils.h"
#import "RSServiceInteractor.h"

#import "TSMessage.h"
#import "RNActivityView.h"
#import "UIView+RNActivityView.h"


@interface RSLoginViewController ()
{
    NSMutableArray *loginArray;

}
@end

static NSString *RSUserSection = @"LoginSection";
static NSString *RSUserName = @"Enter Phone Number";
static NSString *RSPassword =@"Enter Password";
static NSString *RSUserNameMessage =@"Enter UserName";
static NSString *RSPasswordMessage   = @"Enter UserPassword";

@implementation RSLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self intialSetUp];
    self.title = @"Login";
}

-(void)intialSetUp
{
    self.contents=[[NSMutableDictionary alloc]init];
    loginArray=[[NSMutableArray alloc]init];
    [loginArray addObject:[NSMutableDictionary dictionaryWithObjects:@[@"",RSUserName,RSUserNameMessage,@YES] forKeys:@[RSValue,RSTittle,RSMessage,RSTextFieldEnabled]]];
   [loginArray addObject:[NSMutableDictionary dictionaryWithObjects:@[@"",RSPassword,RSPasswordMessage,@YES] forKeys:@[RSValue,RSTittle,RSMessage,RSTextFieldEnabled]]];
    self.sections=@[RSUserSection];
    [self.contents setObject:loginArray forKey:RSUserSection];
    
}

#pragma mark - UITableViewDataSource
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.sections.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSString *sectionTitle=[self.sections objectAtIndex:section];
    return [[self.contents objectForKey:sectionTitle] count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *placeHolder=[self getTitleForIndexPath:indexPath];
    static NSString *loginCellIdentifier=@"RSLoginCell";
    RSLoginCell *loginCell=[tableView dequeueReusableCellWithIdentifier:loginCellIdentifier forIndexPath:indexPath];
    loginCell.selectionStyle = UITableViewCellSelectionStyleNone;
    loginCell.textField.placeholder=placeHolder;
    loginCell.textField.delegate = self;
    if ([placeHolder isEqualToString:RSUserName]) {
        loginCell.textField.keyboardType = UIKeyboardTypeNumberPad;
    }else{
        loginCell.textField.keyboardType = UIKeyboardTypeDefault;
        loginCell.textField.secureTextEntry = YES;
    }
    return loginCell;
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 95;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *fotterView=[[UIView alloc] initWithFrame:CGRectMake(0,0, self.tableView.frame.size.width, 130)];
    fotterView.backgroundColor=[UIColor clearColor];

    UIButton *loginButton=[[UIButton alloc] initWithFrame:CGRectMake(10, 10, self.tableView.frame.size.width-20, 40)];
    loginButton.backgroundColor=[UIColor RSOrangeColour];
    [loginButton addTarget:self action:@selector(loginButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [loginButton setTitle:@"Confirm" forState:UIControlStateNormal];
    [loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    loginButton.layer.cornerRadius = 10.0;
    UIButton *forgotPassWord=[[UIButton alloc] initWithFrame:CGRectMake(0, loginButton.frame.origin.y+50, 130,20)];
    forgotPassWord.backgroundColor=[UIColor clearColor];
    [forgotPassWord addTarget:self action:@selector(forgotPassWordAction:) forControlEvents:UIControlEventTouchUpInside];
    //New Code for forgotPassword button
    NSMutableAttributedString *forgotPasswordTitle = [[NSMutableAttributedString alloc]initWithString:@"Forgot Password?"];
    [forgotPasswordTitle addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, forgotPasswordTitle.length)];
    [forgotPasswordTitle addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(0, forgotPasswordTitle.length)];
    [forgotPasswordTitle addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, forgotPasswordTitle.length)];
    [forgotPassWord setAttributedTitle:forgotPasswordTitle forState:UIControlStateNormal];
    UIButton *registerButton=[[UIButton alloc] initWithFrame:CGRectMake(self.tableView.frame.size.width/2-40,forgotPassWord.frame.origin.y+20 , 100, 25)];
    [registerButton addTarget:self action:@selector(registerButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    NSMutableAttributedString *registerButtonTitle = [[NSMutableAttributedString alloc]initWithString:@"Register?"];
    [registerButtonTitle addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, registerButtonTitle.length)];
    [registerButtonTitle addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(0, registerButtonTitle.length)];
    [registerButtonTitle addAttribute:NSForegroundColorAttributeName value:[UIColor RSOrangeColour] range:NSMakeRange(0, registerButtonTitle.length)];
    [registerButton setAttributedTitle:registerButtonTitle forState:UIControlStateNormal];
    [fotterView addSubview:registerButton];
    [fotterView addSubview:loginButton];
    [fotterView addSubview:forgotPassWord];
    return fotterView;
}

#pragma mark - ButtonActionMethods

-(IBAction)loginButtonAction:(id)sender{
    [self resignKeyBoard];
    NSString *phoneNumber;
    NSString *password;
    for (int i = 0; i< [[self.contents objectForKey:RSUserSection] count]; i++) {
        NSDictionary *dict = [[self.contents objectForKey:RSUserSection] objectAtIndex:i];
        if ([[dict objectForKey:RSTittle] isEqualToString:RSUserName]) {
            phoneNumber = [dict objectForKey:RSValue];
        }
        if ([[dict objectForKey:RSTittle] isEqualToString:RSPassword]){
            password = [dict objectForKey:RSValue];
        }
    }
    if ([PageUtils validateMobileNumber:phoneNumber]) {
        if (password.length>0){
            [self.view showCustomActivityView];
            NSString *urlString = [NSString stringWithFormat:@"mobile=%@&password=%@",phoneNumber,password];
            [RSServiceInteractor signApiCallWith:urlString withCompetion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger responsecode) {
                 [self.view hideActivityView];
                if (success) {
                    NSLog(@"sanjay : %@",dataDict);
                    if ([message isEqualToString:@"Everything worked as expected"]) {
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults setBool:YES forKey:@"Login"];
//                        [defaults setBool:@"YES" forKey:@"Login"];
                        [defaults setObject:[dataDict objectForKey:@"mobile"] forKey:@"mobile"];
                        [defaults setObject:[dataDict objectForKey:@"name"] forKey:@"name"];
                        [defaults setObject:[dataDict objectForKey:@"userId"] forKey:@"userId"];
                        [defaults synchronize];
                        SWRevealViewController *homeVC=[self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                        [self.navigationController presentViewController:homeVC animated:YES completion:nil];
                        [self showSuccessMessage:message];
            }
                }
                else if (responsecode ==232){
                    [self showErrorMessage:@"Invalid Password"];
                }
                else{
                    [self showErrorMessage:@"Mobile Number Does Not Exist. Please Register."];
                }
            }];
        }
        else{
            [self showErrorMessage:@"Please Enter Password"];
        }
    }else {
        [self showErrorMessage:@"Please Enter User Mobile No"];
    }
}

-(IBAction)forgotPassWordAction:(id)sender{
    RSForgotViewController *forgotView = [self.storyboard instantiateViewControllerWithIdentifier:@"RSForgotViewController"];
    [self.navigationController pushViewController:forgotView animated:YES];
}
-(IBAction)registerButtonAction:(id)sender{
    RSignUpViewController *signUpView=[self.storyboard instantiateViewControllerWithIdentifier:@"RSignUpViewController"];
    [self.navigationController pushViewController:signUpView animated:YES];
    
}
-(IBAction)cancelButtonAction:(id)sender{
    
}

#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSIndexPath *indexPath = [self indexPathForView:textField];
    NSString *title = [self getTitleForIndexPath:indexPath];
    if ( [title isEqualToString:RSUserName]){
        return [PageUtils phoneNumberWithRange:range withString:string withlengh:10 withTextField:textField.text];
    }
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (![self.tableView focusNextTextField]) {
        [textField resignFirstResponder];
    }
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSIndexPath *indexPath = [self indexPathForView:textField];
    NSString *type = [self.sections objectAtIndex:indexPath.section];
    id content = [[self.contents objectForKey:type] objectAtIndex:indexPath.row];
    if ([content isKindOfClass:[NSDictionary class]]) {
    [(NSDictionary*)content setValue:textField.text forKey:RSValue];
    }
}

-(void)resignKeyBoard{
    [[self.tableView TPKeyboardAvoiding_findFirstResponderBeneathView:self.view] resignFirstResponder];
}


#pragma mark - healperMethods 

-(NSIndexPath*)indexPathForView:(UIView*)view{
    while (view && ![view isKindOfClass:[UITableViewCell class]])
        view = view.superview;
    return [self.tableView indexPathForRowAtPoint:view.center];
}

-(NSString*)getTitleForCurrentTextfield{
    UITextField *textfield =  (UITextField*)[self.tableView TPKeyboardAvoiding_findFirstResponderBeneathView:self.view];
    NSIndexPath *indexPath = [self indexPathForView:textfield];
    return [self getTitleForIndexPath:indexPath];
}
-(NSString*)getTitleForIndexPath:(NSIndexPath*)indexPath
{
    NSString *type = [self.sections objectAtIndex:indexPath.section];
    id content = [[self.contents objectForKey:type] objectAtIndex:indexPath.row];
    if ([content isKindOfClass:[NSDictionary class]]) {
        return [(NSDictionary*)content objectForKey:RSTittle];
    }else{
        return (NSString*)content;
    }
}

-(NSString*)getValueForIndexPath:(NSIndexPath*)indexPath{
    NSString *type = [self.sections objectAtIndex:indexPath.section];
    id content = [[self.contents objectForKey:type] objectAtIndex:indexPath.row];
    if ([content isKindOfClass:[NSDictionary class]]) {
        return [(NSDictionary*)content objectForKey:RSValue];
    }else{
        return (NSString*)content;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - AlertMessages
-(void)showSuccessMessage:(NSString *)tittle{
    [TSMessage showNotificationWithTitle:@"Login" subtitle:tittle type:TSMessageNotificationTypeSuccess];
}

-(void)showErrorMessage:(NSString *)tittle{
    [TSMessage showNotificationWithTitle:@"Login" subtitle:tittle type:TSMessageNotificationTypeError];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
