//
//  RSResetPasswordViewController.m
//  RideStar
//
//  Created by Raja Sekhar on 14/05/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSResetPasswordViewController.h"
#import "RSLoginViewController.h"

#import "RSServiceInteractor.h"
#import "RSLoginCell.h"
#import "ZMConstant.h"
#import "UIColor+Zigy.h"

#import "RSLoginModel.h"

#import "TSMessage.h"
#import "RNActivityView.h"
#import "UIView+RNActivityView.h"


static NSString *RSUserSection = @"PasswordSection";
static NSString *RSOTP = @"OTP";
static NSString *RSPassword =@"Password";
static NSString *RSConfirmPassword = @"Confirm Password";

static NSString *RSOTPMessage =@"Enter Password";
static NSString *RSPasswordMessage   = @"Enter Password";
static NSString *RSConfirmPasswordMsg = @" Confirm Your Password";


@interface RSResetPasswordViewController ()
{
NSMutableArray *passwordArray;
}
@end

@implementation RSResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self intialSetUp];
    self.title  = @"Reset Password";
    self.view.backgroundColor = [UIColor RSBackgroundColour];
}
-(void)intialSetUp
{
    self.contents=[[NSMutableDictionary alloc]init];
    passwordArray=[[NSMutableArray alloc]init];
   [passwordArray addObject:[NSMutableDictionary dictionaryWithObjects:@[@"",RSOTP,RSOTPMessage,@YES] forKeys:@[RSValue,RSTittle,RSMessage,RSTextFieldEnabled]]];
    [passwordArray addObject:[NSMutableDictionary dictionaryWithObjects:@[@"",RSPassword,RSPasswordMessage,@YES] forKeys:@[RSValue,RSTittle,RSMessage,RSTextFieldEnabled]]];
     [passwordArray addObject:[NSMutableDictionary dictionaryWithObjects:@[@"",RSConfirmPassword,RSConfirmPasswordMsg,@YES] forKeys:@[RSValue,RSTittle,RSMessage,RSTextFieldEnabled]]];
    self.sections=@[RSUserSection];
    [self.contents setObject:passwordArray forKey:RSUserSection];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.sections.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSString *sectionTitle=[self.sections objectAtIndex:section];
    return [[self.contents objectForKey:sectionTitle] count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *placeHolder=[self getTitleForIndexPath:indexPath];
    static NSString *loginCellIdentifier=@"RSLoginCell";
    RSLoginCell *loginCell=[tableView dequeueReusableCellWithIdentifier:loginCellIdentifier forIndexPath:indexPath];
    loginCell.selectionStyle = UITableViewCellSelectionStyleNone;
    loginCell.textField.delegate=self;
    loginCell.textField.placeholder=placeHolder;
    if ([placeHolder isEqualToString:RSOTP]) {
        loginCell.textField.text = UIKeyboardTypeDefault;
    }
    else if ([placeHolder isEqualToString:RSPassword]){
        loginCell.textField.text = UIKeyboardTypeDefault;
        loginCell.textField.secureTextEntry = YES;
    }
    else if([placeHolder isEqualToString:RSConfirmPassword]){
        loginCell.textField.text = UIKeyboardTypeDefault;
        loginCell.textField.secureTextEntry = YES;
    }
    return loginCell;
}
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 48;
//}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
        return 100;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
        UIView *fotterView=[[UIView alloc] initWithFrame:CGRectMake(0,0, self.tableView.frame.size.width, 100)];
        fotterView.backgroundColor=[UIColor whiteColor];
        
        UIButton *loginButton=[[UIButton alloc] initWithFrame:CGRectMake(10, 30, self.tableView.frame.size.width-20, 40)];
        loginButton.backgroundColor=[UIColor RSOrangeColour];
        loginButton.layer.cornerRadius = 20.f;
        loginButton.clipsToBounds = YES;
        [loginButton addTarget:self action:@selector(resetButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [loginButton setTitle:@"Confirm" forState:UIControlStateNormal];
        [loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         [fotterView addSubview:loginButton];
         
        return fotterView;
}


#pragma mark - ButtonActionMethods

-(IBAction)resetButtonAction:(id)sender{
    [self resignKeyBoard];
    
    NSString *OTPNumber;
    NSString *confirmPwd;
    NSString *password;
  
    
    
    for (int i = 0; i< [[self.contents objectForKey:RSUserSection] count]; i++) {
        NSDictionary *dict = [[self.contents objectForKey:RSUserSection] objectAtIndex:i];
        NSLog(@"reset Data:%@",dict);
        if ([[dict objectForKey:RSTittle] isEqualToString:RSOTP]) {
            OTPNumber = [dict objectForKey:RSValue];
            NSLog(@"otp: %@",OTPNumber);
        }
        if ([[dict objectForKey:RSTittle] isEqualToString:RSPassword]){
            password = [dict objectForKey:RSValue];
            NSLog(@"password:%@",password);
        }
        if ([[dict objectForKey:RSTittle]isEqualToString:RSConfirmPassword]) {
            confirmPwd = [dict objectForKey:RSValue];
            NSLog(@"password:%@",confirmPwd);
        }
    }
    if (OTPNumber.length>0) {
        if (password.length>0) {
            if (confirmPwd.length>0) {
                if (password==confirmPwd) {
                    [self.view showCustomActivityView];
                    NSString *url = [NSString stringWithFormat:@"mobile=%@&sid=%@&code=%@&password=%@", [RSLoginModel sharedInstance].mobileNumber,[RSLoginModel sharedInstance].sidValue,OTPNumber,password];
                    NSLog(@"url string passwordreset: %@", url);
                    [RSServiceInteractor forgotPasswordAPI:url withCompetion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
                        [self.view hideActivityView];
                        if (success) {
                            [self showSuccessMessage:@"Password has been reset Successfully"];
                            RSLoginViewController *loginVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"RSLoginViewController"];
                            [self.navigationController pushViewController:loginVC animated:YES];
                            
                        }
                        else if (code==228){
                            [self showErrorMessage:message];
                        }
                        else{
                            [self showErrorMessage:@"Session Expired"];
                        }
                    }];
                }
                else{
                    [self showErrorMessage:@"Password and Confirm Password doesn't match. Please try again"];
                }
            }else{
                [self showErrorMessage:@"Please Enter ConfirmPassword"];
            }
        }else{
            [self showErrorMessage:@"Please Enter Password"];
        }
        
    }else{
        [self showErrorMessage:@"Please Enter OTP"];
    }
}

#pragma mark - healperMethods

-(NSIndexPath*)indexPathForView:(UIView*)view{
    while (view && ![view isKindOfClass:[UITableViewCell class]])
        view = view.superview;
    return [self.tableView indexPathForRowAtPoint:view.center];
}

-(NSString*)getTitleForCurrentTextfield{
    UITextField *textfield =  (UITextField*)[self.tableView TPKeyboardAvoiding_findFirstResponderBeneathView:self.view];
    NSIndexPath *indexPath = [self indexPathForView:textfield];
    return [self getTitleForIndexPath:indexPath];
}
-(NSString*)getTitleForIndexPath:(NSIndexPath*)indexPath
{
    NSString *type = [self.sections objectAtIndex:indexPath.section];
    id content = [[self.contents objectForKey:type] objectAtIndex:indexPath.row];
    if ([content isKindOfClass:[NSDictionary class]]) {
        return [(NSDictionary*)content objectForKey:RSTittle];
    }else{
        return (NSString*)content;
    }
}

-(NSString*)getValueForIndexPath:(NSIndexPath*)indexPath{
    NSString *type = [self.sections objectAtIndex:indexPath.section];
    id content = [[self.contents objectForKey:type] objectAtIndex:indexPath.row];
    if ([content isKindOfClass:[NSDictionary class]]) {
        return [(NSDictionary*)content objectForKey:RSValue];
    }else{
        return (NSString*)content;
    }
}
-(void)resignKeyBoard{
    [[self.tableView TPKeyboardAvoiding_findFirstResponderBeneathView:self.view] resignFirstResponder];
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (![self.tableView focusNextTextField]) {
        [textField resignFirstResponder];
    }
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSIndexPath *indexPath = [self indexPathForView:textField];
    NSString *type = [self.sections objectAtIndex:indexPath.section];
    id content = [[self.contents objectForKey:type] objectAtIndex:indexPath.row];
    if ([content isKindOfClass:[NSDictionary class]]) {
        [(NSDictionary*)content setValue:textField.text forKey:RSValue];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - AlertMessages
-(void)showSuccessMessage:(NSString *)tittle{
    [TSMessage showNotificationWithTitle:@"Success" subtitle:tittle type:TSMessageNotificationTypeSuccess];
}

-(void)showErrorMessage:(NSString *)tittle{
    [TSMessage showNotificationWithTitle:@"Oops" subtitle:tittle type:TSMessageNotificationTypeError];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
