//
//  RSignUpViewController.h
//  RideStar
//
//  Created by Raja Sekhar on 10/04/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RSSharedViewController.h"
#import "TPKeyboardAvoidingTableView.h"

@interface RSignUpViewController : RSSharedViewController<UITableViewDataSource,UITableViewDelegate, UITextFieldDelegate>
@property (nonatomic , weak) IBOutlet TPKeyboardAvoidingTableView *tableView;
@property (nonatomic , strong) NSArray *sections;
@property (nonatomic , strong) NSMutableDictionary *contents;
@property (nonatomic , weak) IBOutlet UIView *holderView;
@end
