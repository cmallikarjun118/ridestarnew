//
//  RSOTPViewController.m
//  RideStar
//
//  Created by Raja Sekhar on 14/05/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSOTPViewController.h"
#import "RSResetPasswordViewController.h"
#import "RSHomeViewController.h"

#import "SWRevealViewController.h"
#import "RSServiceInteractor.h"
#import "UIColor+Zigy.h"
#import "RSLoginModel.h"

#import "RNActivityView.h"
#import "UIView+RNActivityView.h"
#import "TSMessage.h"

@interface RSOTPViewController ()


@end

@implementation RSOTPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.cancelButton.layer.cornerRadius = 20.0;
    self.submitButton.layer.cornerRadius = 20.0;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - ActionMethods
- (IBAction)cancelBtn:(id)sender {
}


- (IBAction)submitBtn:(id)sender {
    if (_otpTextField.text.length == 6) {
        [self.view showCustomActivityView];
        NSString *urlString = [NSString stringWithFormat:@"mobile=%@&password=%@&email=%@&sid=%@&code=%@&name=%@&promo_code=%@", [RSLoginModel sharedInstance].mobileNumber,[RSLoginModel sharedInstance].password,[RSLoginModel sharedInstance].email,[RSLoginModel sharedInstance].sidValue, self.otpTextField.text,[RSLoginModel sharedInstance].userName,[RSLoginModel sharedInstance].promoCode];
       
        [RSServiceInteractor REGISTERAPIWith:urlString withCompetion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
            NSLog(@"data dict:%@", dataDict);
            [self.view hideActivityView];
            if (code==230) {
                [self showErrorMessage:message];
               
            }
            if (code==001) {
                [self showSuccessMessage:@"Registration completed Succesfully"];
                SWRevealViewController *homeVC=[self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                [self.navigationController presentViewController:homeVC animated:YES completion:nil];
//                RSHomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RSHomeViewController"];
//                [self.navigationController pushViewController:homeVC animated:YES];
            }
        }];
    }
    else{
        [self showErrorMessage:@"OTP Field mandatory"];
    }
}
        
#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - AlertMessages
-(void)showSuccessMessage:(NSString *)tittle{
    [TSMessage showNotificationWithTitle:@"Congratulations" subtitle:tittle type:TSMessageNotificationTypeSuccess];
}

-(void)showErrorMessage:(NSString *)tittle{
    [TSMessage showNotificationWithTitle:@"Oops" subtitle:tittle type:TSMessageNotificationTypeError];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
