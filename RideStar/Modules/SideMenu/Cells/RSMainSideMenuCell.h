//
//  RSMainSideMenuCell.h
//  RideStar
//
//  Created by Raja Sekhar on 10/04/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSMainSideMenuCell : UITableViewCell
@property (nonatomic , weak) IBOutlet UILabel *nameLable;
@property (nonatomic , weak) IBOutlet UIImageView *image;

@end
