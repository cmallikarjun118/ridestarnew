//
//  RSMainMenuViewController.m
//  RideStar
//
//  Created by Raja Sekhar on 05/04/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSMainMenuViewController.h"
#import "RSMainSideMenuCell.h"

#import "RSLoginViewController.h"
#import "RSHomeViewController.h"
#import "RSBookingHistoryViewController.h"
#import "RSUserProfileViewController.h"
#import "RSCashBack.h"

#import "UIColor+Zigy.h"
#import "RSServiceInteractor.h"


#import "UIImageView+Letters.h"
#import "SWRevealViewController.h"

@interface RSMainMenuViewController ()

@end

@implementation RSMainMenuViewController{
     NSDictionary *promo;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.userNameLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"name"];
    self.namesArray =@[@"Home", @"Profile", @"My Rides",@"Cashback",@"Share",@"Feedback",@"Help",@"Logout"];
    self.imagesArray=@[@"Home",@"Profile",@"MyRides",@"CashBack",@"Share",@"FeedBack",@"Help",@"Logout"];
    [self.profileImage setImageWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"name"] color:[UIColor RSOrangeColour] circular:YES];
    [self getPromo];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - UITableViewDataSource

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.namesArray.count;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier=@"RSMainSideMenuCell";
    RSMainSideMenuCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.nameLable.text=[self.namesArray objectAtIndex:indexPath.row];
    cell.nameLable.textColor = [UIColor RSOrangeColour];
    cell.image.image=[UIImage imageNamed:[self.imagesArray objectAtIndex:indexPath.row]];
    return cell;
}
#pragma mark - UITableViewDelegate

//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    UIView *headerView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 100)];
//    UIImageView *imagView =[[UIImageView alloc] initWithFrame:CGRectMake(5, 5,headerView.frame.size.height-10 , headerView.frame.size.height-10)];
//  //  imagView.layer.cornerRadius = headerView.frame.size.height-10;
//    //imagView.layer.borderColor = [UIColor RSYellowColour].CGColor;
//  //  imagView.layer.borderWidth = 2.0;
//  //  imagView.clipsToBounds = YES ;
//    imagView.image = [UIImage imageNamed:@"Userprofile"];
//    UILabel *userName = [[UILabel alloc] initWithFrame:CGRectMake(headerView.frame.size.height-10, 20, headerView.frame.size.width-headerView.frame.size.height-10,30 )];
//    userName.text = @"Guest User";
//    [headerView addSubview:imagView];
//    [headerView addSubview:userName];
//    return headerView;
//}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *string = [self.namesArray objectAtIndex:indexPath.row];
    SWRevealViewController *reavealController=(SWRevealViewController *)self.revealViewController;
    if ([string isEqualToString:@"Home"]) {
        RSHomeViewController *frontViewConroller=[self.storyboard instantiateViewControllerWithIdentifier:@"RSHomeViewController"];
        UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:frontViewConroller];
        [reavealController setFrontViewController:nav];
        [reavealController setFrontViewPosition:FrontViewPositionLeft animated:YES];
        
    }else if ([string isEqualToString:@"Profile"]){
        RSUserProfileViewController *rsProfileConroller=[self.storyboard instantiateViewControllerWithIdentifier:@"RSUserProfileViewController"];
        UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:rsProfileConroller];
        [reavealController setFrontViewController:nav];
        [reavealController setFrontViewPosition:FrontViewPositionLeft animated:YES];
        
    }else if ([string isEqualToString:@"My Rides"]){
        RSBookingHistoryViewController *frontViewConroller=[self.storyboard instantiateViewControllerWithIdentifier:@"RSBookingHistoryViewController"];
        UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:frontViewConroller];
        [reavealController setFrontViewController:nav];
        [reavealController setFrontViewPosition:FrontViewPositionLeft animated:YES];
        
    }else if ([string isEqualToString:@"Cashback"]){
        RSCashBack *frontViewConroller=[self.storyboard instantiateViewControllerWithIdentifier:@"RSCashBack"];
        UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:frontViewConroller];
        [reavealController setFrontViewController:nav];
        [reavealController setFrontViewPosition:FrontViewPositionLeft animated:YES];
        
    }else if ([string isEqualToString:@"Feedback"]){
        mailComposer = [[MFMailComposeViewController alloc] init];
        mailComposer.mailComposeDelegate = self;
        
        [mailComposer setSubject:@"Feedback_RideStar"];
        
        [mailComposer setToRecipients:@[@"sanjaykbharadwaz@gmail.com"]];
        
        [self presentViewController:mailComposer animated:YES completion:nil];
        
    }else if ([string isEqualToString:@"Share"]){
        
        NSMutableArray *sharingItems = [NSMutableArray new];
        NSString *shareContent=[NSString stringWithFormat:@"Use my Hoppi promocode %@, and get Rs.%@ cashback. Redeem it at https:hoppi.in", [promo valueForKey:@"promo_code"],[promo valueForKey:@"amount"]];
        NSLog(@"%@",shareContent);
        [sharingItems addObject:shareContent];
        UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
        [self presentViewController:activityController animated:YES completion:nil];
        
        
    }else if ([string isEqualToString:@"Logout"] || [string isEqualToString:@"Logout"]){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults removeObjectForKey:@"Login"];
        [defaults setBool:@"YES" forKey:@"Logout"];
        RSLoginViewController *frontViewConroller=[self.storyboard instantiateViewControllerWithIdentifier:@"RSLoginViewController"];
        UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:frontViewConroller];
        [reavealController setFrontViewController:nav];
        [reavealController setFrontViewPosition:FrontViewPositionLeft animated:YES];
    }
}

-(void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:{
            
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Turn on ur mail in settings"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];}
            break;
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)getPromo{
    NSString *urlPath = [NSString stringWithFormat:@"user_id=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userId"]];
    [RSServiceInteractor GetPromoCode:urlPath withCompletion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        promo = dataDict;
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
