//
//  RSMainMenuViewController.h
//  RideStar
//
//  Created by Raja Sekhar on 05/04/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface RSMainMenuViewController : UIViewController<UITableViewDelegate, UITableViewDataSource,MFMailComposeViewControllerDelegate>
{
    MFMailComposeViewController *mailComposer;
    
}

@property (nonatomic , weak) IBOutlet UITableView *tableView;
@property (nonatomic , strong) NSArray *namesArray;
@property (nonatomic , strong) NSArray *imagesArray;

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;


@end
