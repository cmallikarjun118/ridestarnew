//
//  PageUtils.m
//  RideStar
//
//  Created by Medex-ios on 8/14/15.
//  Copyright (c) 2015 Mobi Esprits Software Pvt. Ltd. All rights reserved.
//

#import "PageUtils.h"

@implementation PageUtils

+(BOOL)validateEmailWithString:(NSString *)email{
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    if ([emailTest evaluateWithObject:email] == YES)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
}
+(BOOL)validateForWhiteSpaceAndSpecialCharacters:(NSString *)string
{
    
    NSString *rawString = string;
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed = [rawString stringByTrimmingCharactersInSet:whitespace];
    //  NSString *str = string;
    NSString *specialCharacter = string;
    NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"!~`@#$%^&*-+();:={}[],.<>?\\/\"\'"];
    specialCharacter = [[specialCharacter componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
    NSLog(@"%@", specialCharacter); // => foobarbazfoo
    NSString *trimmed1 = [specialCharacter stringByTrimmingCharactersInSet:whitespace];
    
    //  NSCharacterSet *alphaSet = [NSCharacterSet alphanumericCharacterSet];
    //   BOOL valid = [[trimmed stringByTrimmingCharactersInSet:alphaSet] isEqualToString:@""];
    if ([trimmed length] == 0 || [trimmed1 length]==0) {
        NSLog(@"only white spaces are ther");
        return YES;
    }
    return NO;
}


+(BOOL)validateMobileNumber:(NSString*)number
{
    NSString *numberRegEx = @"[0-9]{10}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    if ([numberTest evaluateWithObject:number] == YES)
        return TRUE;
    else
        return FALSE;
}

+(BOOL)phoneNumberWithRange:(NSRange)range withString:(NSString *)string withlengh:(NSUInteger)integer withTextField:(NSString *)textfield{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARECTERS] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    NSString *updatedText = [textfield stringByReplacingCharactersInRange:range withString:string];
    if (updatedText.length > integer){
        return NO;
    }
    return [string isEqualToString:filtered];
}


@end
