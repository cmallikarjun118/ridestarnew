//
//  PageUtils.h
//  RideStar
//
//  Created by Medex-ios on 8/14/15.
//  Copyright (c) 2015 Mobi Esprits Software Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ZMConstant.h"
@interface PageUtils : NSObject
+(BOOL)validateEmailWithString:(NSString *)email;
+(BOOL)validateForWhiteSpaceAndSpecialCharacters:(NSString *)string;
+(BOOL)validateMobileNumber:(NSString*)number;
+(BOOL)phoneNumberWithRange:(NSRange)range withString:(NSString *)string withlengh:(NSUInteger)integer withTextField:(NSString *)textfield;

@end
