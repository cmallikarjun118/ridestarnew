//
//  ZMConstant.h
//  Zigy
//
//  Created by Yogesh Veeraraj on 04/02/16.
//  Copyright © 2016 Yogesh Veeraraj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#ifndef ZMConstant_h
#define ZMConstant_h

#define SCREEN_WIDTH    ([UIScreen mainScreen].bounds.size.width)
#define appDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

typedef void(^Completion)(void);

typedef NS_ENUM(NSInteger, PickerType)
{
    eFormType = 0,
    ePotencyType,
    eQuantityType
};


//Aspect Ratio size
static const CGSize kMainBannerSize = {694, 616};
static const CGSize kPromoBannerSize = {686,288};
static const CGSize kSubBannerSize = {767,350};

//Base URL
extern NSString* const kBaseUrl;
extern NSString* const kWebBaseUrl;


extern  NSString*  const RSTittle;
extern  NSString*  const RSValue;
extern  NSString*  const RSTextFieldEnabled;
extern  NSString*  const RSMessage;
extern  NSString*  const kOffers;
extern  NSString*  const kViewHistory;
extern  NSString*  const kRateZigyApp;
extern  NSString*  const kChangePassword;
extern  NSString*  const kLogout;
extern  NSString*  const kTermsandConditions;
extern  NSString*  const kFaqsandQueries;
extern  NSString*  const kName;

extern NSString* const kMainSection;
extern NSString* const KPromoSection;
extern NSString* const kBrandsSection;
extern NSString* const kMainCategorySection;

//StoryBoards

extern NSString* const kProductDetail;
extern NSString* const kTagsTableCellReuseIdentifier;


//User Defaults
extern  NSString* const kInititalDownloadKey;
extern  NSString* const kWellnessDataSource;
extern  NSString* const kHomeFormat;

//Table header
extern  NSString* const kHeaderView;

//add to cart View

extern NSString*  const kAddToCartFooter;
extern NSString*  const ACCEPTABLE_CHARECTERS;

//profile Header fotter
extern NSString* const kPofileHeaderFotter;

//googleMaps key

extern NSString* const GoogleMapsApiKey;
extern NSString* const GooglePlacesApiKey;

//WebUrls

extern NSString* const olaWebUrl;
extern NSString* const uberWebUrl;



#endif /* ZMConstant_h */
