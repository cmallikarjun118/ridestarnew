//
//  RSUserProfileViewController.m
//  RideStar
//
//  Created by Raja Sekhar on 14/04/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSUserProfileViewController.h"

#import "RSProfileCell.h"

#import "PageUtils.h"
#import "ZMConstant.h"
#import "UIColor+Zigy.h"
#import "RSServiceInteractor.h"
#import "UIViewController+RScommonSettings.h"

#import "SWRevealViewController.h"
#import "RNActivityView.h"
#import "UIView+RNActivityView.h"
#import "TSMessage.h"


static NSString *userProfile = @"UserProfile";

static NSString *kEmail = @"Email Address";
static NSString *kPhoneNo = @"Contact Number";
static NSString *kPassword = @"Password";





@interface RSUserProfileViewController ()

{
    NSMutableArray *userProfileArray;
    NSDictionary *userDetails;
}
@property(nonatomic,strong) UIBarButtonItem *leftBarButton;

@end

@implementation RSUserProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self navigationBarSetting];
    [self getUserDetails];
 
    self.view.backgroundColor = [UIColor RSBackgroundColour];
    self.UserNameLabel.textColor = [UIColor RSOrangeColour];
    self.saveBtn.layer.cornerRadius = self.saveBtn.frame.size.height/2;
    
    
}



-(void)buildSections:(BOOL)editable{
    self.contents=[[NSMutableDictionary alloc]init];
    userProfileArray=[[NSMutableArray alloc]init];
    [userProfileArray addObject:[NSMutableDictionary dictionaryWithObjects:@[[[userDetails objectForKey:@"mobile"]floatValue]>0?[userDetails objectForKey:@"mobile"]:@"",kPhoneNo,@"",editable?@YES:@NO] forKeys:@[RSValue,RSTittle,RSMessage,@""]]];
     [userProfileArray addObject:[NSMutableDictionary dictionaryWithObjects:@[[userDetails objectForKey:@"email"]?[userDetails objectForKey:@"email"]:@"",kEmail,@"",editable?@YES:@NO] forKeys:@[RSValue,RSTittle,RSMessage,RSTextFieldEnabled]]];
    [userProfileArray addObject:[NSMutableDictionary dictionaryWithObjects:@[@"",kPassword,@"",editable?@YES:@NO] forKeys:@[RSValue,RSTittle,RSMessage,RSTextFieldEnabled]]];
    
    self.sections=@[userProfile];
    [self.contents setObject:userProfileArray forKey:userProfile];
}
#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.sections.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSString *sectionTitle=[self.sections objectAtIndex:section];
    return [[self.contents objectForKey:sectionTitle] count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *profileIdentifier = @"RSProfileCell";
    NSString *titlle = [self getTitleForIndexPath:indexPath];
    NSString *value  = [self getValueForIndexPath:indexPath];
    RSProfileCell *profilecell = [tableView dequeueReusableCellWithIdentifier:profileIdentifier forIndexPath:indexPath];
    profilecell.typeLabel.text = titlle;
    profilecell.textField.delegate = self;
    profilecell.selectionStyle = UITableViewCellSelectionStyleNone;
    if ([titlle isEqualToString:kPassword]) {
        profilecell.textField.text = @"NO password value in response";
        profilecell.textField.secureTextEntry=YES;
        profilecell.textField.userInteractionEnabled = NO;
    
    }
    else if ([titlle isEqualToString:kPhoneNo]) {
        profilecell.textField.userInteractionEnabled = NO;
        profilecell.textField.text = value;
    }
    else if ([titlle isEqualToString:kEmail]){
        profilecell.textField.keyboardType = UIKeyboardTypeEmailAddress;
        profilecell.userInteractionEnabled = YES;
        profilecell.textField.text = value;
    }
    return profilecell;
}
#pragma mark - UITableViewDelegate


#pragma mark -UITextFieldDelegate

-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSIndexPath *indexPath = [self indexPathForView:textField];
    NSString *type = [self.sections objectAtIndex:indexPath.section];
    id content = [[self.contents objectForKey:type] objectAtIndex:indexPath.row];
    if ([content isKindOfClass:[NSDictionary class]]) {
        [(NSDictionary*)content setValue:textField.text forKey:RSValue];
    }
}

-(void)resignKeyBoard{
    [[self.tableView TPKeyboardAvoiding_findFirstResponderBeneathView:self.view] resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - HelperMethods

-(NSIndexPath*)indexPathForView:(UIView*)view{
    while (view && ![view isKindOfClass:[UITableViewCell class]])
        view = view.superview;
    return [self.tableView indexPathForRowAtPoint:view.center];
}

-(NSString*)getTitleForCurrentTextfield{
    UITextField *textfield =  (UITextField*)[self.tableView TPKeyboardAvoiding_findFirstResponderBeneathView:self.view];
    NSIndexPath *indexPath = [self indexPathForView:textfield];
    return [self getTitleForIndexPath:indexPath];
}

-(NSString*)getValueForIndexPath:(NSIndexPath*)indexPath{
    NSString *type = [self.sections objectAtIndex:indexPath.section];
    id content = [[self.contents objectForKey:type] objectAtIndex:indexPath.row];
    if ([content isKindOfClass:[NSDictionary class]]) {
        return [(NSDictionary*)content objectForKey:RSValue];
    }else{
        return (NSString*)content;
    }
}

-(NSString*)getTitleForIndexPath:(NSIndexPath*)indexPath
{
    NSString *type = [self.sections objectAtIndex:indexPath.section];
    id content = [[self.contents objectForKey:type] objectAtIndex:indexPath.row];
    if ([content isKindOfClass:[NSDictionary class]]) {
        return [(NSDictionary*)content objectForKey:RSTittle];
    }else{
        return (NSString*)content;
    }
}

#pragma mark - API call

-(void)getUserDetails{
    [self.view showCustomActivityView];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userId=[defaults objectForKey:@"userId"];
    NSString *url = [NSString stringWithFormat:@"userId=%@&action=get", userId];
    [RSServiceInteractor userAction:url withCompetion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        [self.view hideActivityView];
        if (code==001) {
            userDetails = dataDict;
            [self buildSections:NO];
            NSLog(@"%@",dataDict);
            [self.tableView reloadData];
            self.UserNameLabel.text = [userDetails valueForKey:@"name"];
        }
    }];
}


- (IBAction)saveButtonAction:(id)sender {
    NSString *emailId;
   // NSString *userName;
    for (int i = 0; i< [[self.contents objectForKey:userProfile] count]; i++) {
        NSDictionary *dict = [[self.contents objectForKey:userProfile] objectAtIndex:i];
        if ([[dict objectForKey:RSTittle] isEqualToString:kEmail]) {
            emailId = [dict objectForKey:RSValue];
            NSLog(@"email id:%@",emailId);

        }
//        if ([[dict objectForKey:RSTittle] isEqualToString:kPassword]){
//            userName = [dict objectForKey:RSValue];
//        }
    }
    if ([PageUtils validateEmailWithString:emailId]) {
        NSLog(@"email id:%@",emailId);
        NSString *url = [NSString stringWithFormat:@"userId=%@&action=update&email=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userId"],emailId];
        NSString *encode = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [RSServiceInteractor userAction:encode withCompetion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
            [self.view hideActivityView];
            if (success) {
                [self showSuccessMessage:@"Your Profile has been updated succesfully"];
                [self getUserDetails];
            }
        }];
    }
    else{
        [self showErrorMessage:@"Enter Valid Email"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - AlertMessages
-(void)showSuccessMessage:(NSString *)tittle{
    [TSMessage showNotificationWithTitle:@"Success" subtitle:tittle type:TSMessageNotificationTypeSuccess];
}

-(void)showErrorMessage:(NSString *)tittle{
    [TSMessage showNotificationWithTitle:@"Oops" subtitle:tittle type:TSMessageNotificationTypeError];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
