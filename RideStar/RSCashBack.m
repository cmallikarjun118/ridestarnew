//
//  RSCashBack.m
//  RideStar
//
//  Created by sanjay on 20/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSCashBack.h"

#import "RSOffersCollectionViewCell.h"

#import "RSServiceInteractor.h"
#import "UIViewController+RScommonSettings.h"
#import "UIColor+Zigy.h"


#import <SDImageCache.h>
#import <SDWebImage/UIImageView+WebCache.h>


@implementation RSCashBack
-(void)viewDidLoad{
    [super viewDidLoad];
    [self navigationBarSetting];
    [self getOffersDetails];
    self.title=@" My Cashback";
    self.view.backgroundColor = [UIColor RSBackgroundColour];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"RSOffersCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"RSOffersCollectionViewCell"];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark:<collectionView Data source>
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(self.collectionView.frame.size.width,self.collectionView.frame.size.height-10);
    //    return CGSizeMake(self.collectionView.frame.size.width, self.view.frame.size.height-(self.bookingConfirmationView.frame.size.height+self.pageController.frame.size.height+60));
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    RSOffersCollectionViewCell  *offersCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RSOffersCollectionViewCell" forIndexPath:indexPath];
    offersCell.contentView.layer.borderColor=[UIColor RSOrangeColour].CGColor ;
    offersCell.contentView.layer.borderWidth=2.0;
    offersCell.contentView.layer.cornerRadius=10.0;
    offersCell.contentView.clipsToBounds=YES;
    
    
    
    self.dict=[self.dealsArray objectAtIndex:indexPath.row];
    [offersCell setData:self.dict];
    [offersCell.shopButton addTarget:self action:@selector(shopButtonAction) forControlEvents:UIControlEventTouchDown];
    [offersCell.shopButton addTarget:self action:@selector(laterButtonAction) forControlEvents:UIControlEventTouchDown];
    return offersCell;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dealsArray.count;
}



#pragma mark - Button Actions
-(void)shopButtonAction{
    NSURL *url = [NSURL URLWithString:[self.dict objectForKey:@"link"]];
    if ([[UIApplication sharedApplication]canOpenURL:url]){
        [[UIApplication sharedApplication] openURL:url];
    }
    else
    {
        [self showAlertMessage:@"" withMessage:@"Please Try Again"];
        
    }
    
}

-(void)laterButtonAction{
    
}


#pragma API call

-(void)getOffersDetails{
    NSString *url = @"city=bangalore";
    [RSServiceInteractor GetDeals:url withCompetion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        self.dealsArray = [dataDict mutableCopy];
        NSLog(@"deals array is%@",_dealsArray);
        [self.collectionView reloadData];
    }];
  
}

-(void)showAlertMessage:(NSString *)tittle withMessage:(NSString *)message
{
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:tittle message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
    
}

@end
