//
//  RSCabDetails.h
//  RideStar
//
//  Created by sanjay on 22/06/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSSharedViewController.h"

#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

typedef NS_ENUM(NSInteger,selectedCabType) {
    cabSelected ,
    shareSelected,
    autoSelected,
    shuttleSelected,
};


@interface RSCabDetails : RSSharedViewController<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@property (nonatomic , weak) IBOutlet UITableView *tableView;
@property (nonatomic , weak) IBOutlet UIView *cabTypesHolderView;
@property (nonatomic , weak) IBOutlet UILabel *distanceLabel;
@property (nonatomic , weak) IBOutlet UIButton *cabButton , *shareButton , *autoButton , *shuttlButton;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (nonatomic , strong) NSArray *cabsList;
@property (nonatomic) selectedCabType selectedType;
- (IBAction)cabsButtonAction:(id)sender;
- (IBAction)shareButtonAction:(id)sender;
- (IBAction)autoButtonAction:(id)sender;
- (IBAction)shuttleAction:(id)sender;

@end
