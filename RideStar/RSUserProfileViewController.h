//
//  RSUserProfileViewController.h
//  RideStar
//
//  Created by Raja Sekhar on 14/04/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+Zigy.h"
#import "TPKeyboardAvoidingTableView.h"


@interface RSUserProfileViewController : UIViewController<UITableViewDataSource , UITableViewDelegate,UITextFieldDelegate>

@property (nonatomic , weak) IBOutlet UITableView *tableView;
@property (nonatomic , strong) NSArray *sections;
@property (nonatomic , strong) NSMutableDictionary *contents;
@property (weak, nonatomic) IBOutlet UILabel *UserNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;


- (IBAction)saveButtonAction:(id)sender;

@end
