//
//  RSCashBack.h
//  RideStar
//
//  Created by sanjay on 20/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface RSCashBack : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>


@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *cashBackLabel;



@property(nonatomic,strong)NSMutableArray *dealsArray;
@property(nonatomic, strong)NSDictionary *dict;

@end
