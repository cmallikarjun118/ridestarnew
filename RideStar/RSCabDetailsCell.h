//
//  RSCabDetailsCell.h
//  RideStar
//
//  Created by sanjay on 22/06/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSCabDetails.h"

@interface RSCabDetailsCell : UITableViewCell
//@property (weak, nonatomic) IBOutlet UIImageView *companyLogo;
//
//@property (weak, nonatomic) IBOutlet UIImageView *productLogo;
//@property (weak, nonatomic) IBOutlet UILabel *arrivalLabel;
//
//@property (weak, nonatomic) IBOutlet UILabel *totalAmount;
//@property (weak, nonatomic) IBOutlet UILabel *minCost;
//@property (weak, nonatomic) IBOutlet UILabel *costPerKm;
//@property (weak, nonatomic) IBOutlet UILabel *waitningCharge;
//@property (weak, nonatomic) IBOutlet UILabel *surgeCharge;
//@property (weak, nonatomic) IBOutlet UIButton *bookbtn;
@property (weak, nonatomic) IBOutlet UILabel *serviceProviderLabel;
@property (weak, nonatomic) IBOutlet UILabel *cashBackLabel;
@property (weak, nonatomic) IBOutlet UILabel *cabNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;
@property (weak, nonatomic) IBOutlet UIButton *bookNowButton;
@property (weak, nonatomic) IBOutlet UILabel *detailsLabel;

-(void)setData:(NSDictionary *)details;

@end
