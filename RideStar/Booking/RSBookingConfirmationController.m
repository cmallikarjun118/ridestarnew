//
//  RSBookingConfirmationController.m
//  RideStar
//
//  Created by Local user on 12/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//


//static NSString *bookingStatus = @"Your Booking Accepted";

#import "RSBookingConfirmationController.h"
#import "RSServiceInteractor.h"

#import "RSHomeModel.h"

#import "RSCabStatusCell.h"
#import "RSCabDetailsCell.h"
#import "RSOffersCell.h"
#import "RSDriverDetailsCell.h"
#import "RSShuttleDetailsCell.h"


#import "UIColor+Zigy.h"

@interface RSBookingConfirmationController (){
    NSMutableArray *cabDetailsArray;
    NSMutableArray *sectionsArray;
    NSTimer *timer;
}

@end

static NSString *RSBookingConfirm = @"Your Booking is Accepted" ;
static NSString *RSCabStarted = @"Cab Started";
static NSString *RSCabArrived =@"Cab Arrived";
static NSString *RSRideStarted =@"Ride Started";
static NSString *RSOffers  = @"Offers";
static NSString *RSDriverDetails =@"Driver Details";
static NSString *RSCabStatusSection =@"Cab Status";
static NSString *RSCabDeatilsSection =@"Cab Details";
static NSString *RSOffersSection =@"Deals";
static NSString *RSShuttleDetails = @" Shuttle Details";


@implementation RSBookingConfirmationController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Your Ride Status";
    if ([[RSHomeModel sharedInstance].cabType isEqualToString:@"BMTC"] || [[RSHomeModel sharedInstance].cabType isEqualToString:@"METRO"]) {
         sectionsArray = [[NSMutableArray alloc] initWithObjects:RSShuttleDetails,RSOffersSection,nil];
    }else{
    sectionsArray = [[NSMutableArray alloc] initWithObjects:RSCabStatusSection,RSCabDeatilsSection,RSOffersSection,nil];
    timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(trackMyRide) userInfo:nil repeats:YES];
    }
     NSMutableDictionary *bookingDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:RSBookingConfirm,@"Name",@NO,@"isSelected", nil];
      NSMutableDictionary *cabStartedDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:RSCabArrived,@"Name",@NO,@"isSelected", nil];
      NSMutableDictionary *cabArrivedDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:RSCabStarted,@"Name",@NO,@"isSelected", nil];
      NSMutableDictionary *cabStatusDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:RSRideStarted,@"Name",@NO,@"isSelected", nil];
    cabDetailsArray  =[[NSMutableArray alloc] initWithObjects:bookingDict,cabStartedDict,cabArrivedDict,cabStatusDict, nil];
    
    // Do any additional setup after loading the view.
}


#pragma mark -  UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return sectionsArray.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSString *sectionTittle  = [sectionsArray objectAtIndex:section];
    if ([[RSHomeModel sharedInstance].cabType isEqualToString:@"OLA"]||[[RSHomeModel sharedInstance].cabType isEqualToString:@"UBER"]) {
        if ([sectionTittle isEqualToString:RSCabStatusSection]) {
            return cabDetailsArray.count;
        }else
            return 1;
    }
   else{
        return 1;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *sectionTittle  = [sectionsArray objectAtIndex:indexPath.section];
    if ([sectionTittle isEqualToString:RSOffersSection]) {
        return 250;
    }else if ([sectionTittle isEqualToString:RSCabDeatilsSection]){
        return 70;
    }else{
        return 45;
    }

}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setTextAlignment:NSTextAlignmentCenter];
//    [[UIView appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil]setBackgroundColor:[UIColor RSPeachColour]];
    if (section==2) {
        return 0;
    }else
   return [sectionsArray objectAtIndex:section];
    
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *sectionTittle  = [sectionsArray objectAtIndex:indexPath.section];
    static NSString * offersCellIdentifier  = @"RSOffersCell";
    static NSString * statusCellIdentifier  = @"RSCabStatusCell";
    static NSString * driverCellIdentifier    = @"RSDriverDetailsCell";
    static NSString * shuttleCellIdentifier = @"RSShuttleDetailsCell";
    if ([[RSHomeModel sharedInstance].cabType isEqualToString:@"BMTC"] || [[RSHomeModel sharedInstance].cabType isEqualToString:@"METRO"]) {
        if ([sectionTittle isEqualToString:RSOffersSection]) {
            RSOffersCell *offersCell = [tableView dequeueReusableCellWithIdentifier:offersCellIdentifier forIndexPath:indexPath];
            return offersCell;
        }else{
            RSShuttleDetailsCell *shuttle = [self.tableView dequeueReusableCellWithIdentifier:shuttleCellIdentifier forIndexPath:indexPath];
            return shuttle;
        }
    }else if ([[RSHomeModel sharedInstance].cabType isEqualToString:@"OLA"]||[[RSHomeModel sharedInstance].cabType isEqualToString:@"UBER"]){
        if ([sectionTittle isEqualToString:RSCabDeatilsSection]) {
            RSDriverDetailsCell *cabDetailsCell = [tableView dequeueReusableCellWithIdentifier:driverCellIdentifier forIndexPath:indexPath];
            [cabDetailsCell setCabDetails:[RSHomeModel sharedInstance].bookedCabDetails];
            [cabDetailsCell.callButton addTarget:self action:@selector(callNowAction) forControlEvents:UIControlEventTouchUpInside];
            return cabDetailsCell;
            
        }else if ([sectionTittle isEqualToString:RSCabStatusSection]){
            RSCabStatusCell *statusCell = [tableView dequeueReusableCellWithIdentifier:statusCellIdentifier forIndexPath:indexPath];
            statusCell.tag = indexPath.row;
            NSDictionary *dict = [cabDetailsArray objectAtIndex:indexPath.row];
            [statusCell setImageForCabStatus:dict];
            if (statusCell.tag==3) {
                statusCell.lineView.hidden=YES;
            }
            return statusCell;
        }else{
            RSOffersCell *offersCell = [tableView dequeueReusableCellWithIdentifier:@"RSOffersCell" forIndexPath:indexPath];
            return offersCell;
        }

    }else{
        RSOffersCell *offersCell = [tableView dequeueReusableCellWithIdentifier:@"RSOffersCell" forIndexPath:indexPath];
        return offersCell;
        
    }
}
/*
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *sectionTittle  = [sectionsArray objectAtIndex:indexPath.section];
    static NSString * offersIdentifier  = @"RSOffersCell";
    static NSString * statusIdentifer  = @"RSCabStatusCell";
    static NSString * driverDetails    = @"RSDriverDetailsCell";
    
    if ([sectionTittle isEqualToString:RSOffersSection]) {
        RSOffersCell *offersCell = [tableView dequeueReusableCellWithIdentifier:offersIdentifier forIndexPath:indexPath];
        return offersCell;
    }else if([sectionTittle isEqualToString:RSCabDeatilsSection]) {
        RSDriverDetailsCell *cabDetailsCell = [tableView dequeueReusableCellWithIdentifier:driverDetails forIndexPath:indexPath];
        [cabDetailsCell setCabDetails:[RSHomeModel sharedInstance].bookedCabDetails];
        [cabDetailsCell.callButton addTarget:self action:@selector(callNowAction) forControlEvents:UIControlEventTouchUpInside];
        return cabDetailsCell;
    }else{
        RSCabStatusCell *statusCell = [tableView dequeueReusableCellWithIdentifier:statusIdentifer forIndexPath:indexPath];
        statusCell.tag = indexPath.row;
        NSDictionary *dict = [cabDetailsArray objectAtIndex:indexPath.row];
        [statusCell setImageForCabStatus:dict];
        if (statusCell.tag==3) {
            statusCell.lineView.hidden=YES;
        }
        return statusCell;
    }
}*/
#pragma mark - UITableViewDelegate


#pragma mark - ActionMethods

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)callNowAction{
    NSString *phone = [NSString stringWithFormat:@"telprompt://%@",[ [RSHomeModel sharedInstance].bookedCabDetails valueForKey:@"driver_pno"]];
 
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phone] ];
}
-(void)trackMyRide{
    NSString *url = [NSString stringWithFormat:@"user_id=1&service_name=OLA&booking_id=CRN288386674"];
    [RSServiceInteractor trackCabDetails:url withCompletion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (success) {
            NSLog(@"tracking:%@",dataDict);
            for (NSMutableDictionary *dict in cabDetailsArray) {
                if ([[dataDict valueForKey:@"booking_status"] isEqualToString:@"BOOKING_CANCELLED"]) {
                        [timer invalidate];
                    [dict setObject:[NSNumber numberWithBool:YES] forKey:@"isSelected"];
                }else if ([[dataDict valueForKey:@"booking_status"] isEqualToString:@"cabStarted"]){
                    if ([[dict objectForKey:@"Name"] isEqualToString:RSBookingConfirm] || [[dict objectForKey:@"Name"] isEqualToString:RSCabStarted]) {
                        [dict setObject:[NSNumber numberWithBool:YES] forKey:@"isSelected"];
                    }
                }
                else if ([[dataDict valueForKey:@"booking_status"] isEqualToString:@"cabArrived"]){
                    if ([[dict objectForKey:@"Name"] isEqualToString:RSBookingConfirm] || [[dict objectForKey:@"Name"] isEqualToString:RSCabStarted] || [[dict objectForKey:@"Name"] isEqualToString:RSCabArrived]) {
                        [dict setObject:[NSNumber numberWithBool:YES] forKey:@"isSelected"];
                    }
                }
                else if ([[dataDict valueForKey:@"booking_status"] isEqualToString:@"rideStarted"]){
                    if ([[dict objectForKey:@"Name"] isEqualToString:RSBookingConfirm] || [[dict objectForKey:@"Name"] isEqualToString:RSCabStarted] || [[dict objectForKey:@"Name"] isEqualToString:RSCabArrived] || [[dict objectForKey:@"Name"] isEqualToString:RSRideStarted]) {
                        [dict setObject:[NSNumber numberWithBool:YES] forKey:@"isSelected"];
                    }
                }
            }
            NSRange range = NSMakeRange(0, 1);
            NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
            [self.tableView reloadSections:section withRowAnimation:UITableViewRowAnimationAutomatic];
            
        }
    }];
}
-(void)addMyRides{
    NSString *url = [NSString stringWithFormat:@"user_id=%@&ride_date=2016-01-25&ride_time=11.30&provider=%@&product_type=mini&source_location=test+source&destination_location=test+destination&distance=20+km&exact_ride_price=0&ride_status=2",[[NSUserDefaults standardUserDefaults]valueForKey:@"userId"],[RSHomeModel sharedInstance].cabType];
    [RSServiceInteractor addMyRides:url withCompletion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (success) {
            NSLog(@"Data%@",dataDict);
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
