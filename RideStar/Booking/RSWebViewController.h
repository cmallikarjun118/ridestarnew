//
//  RSWebViewController.h
//  RideStar
//
//  Created by Local user on 17/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSSharedViewController.h"

@interface RSWebViewController :RSSharedViewController <UIWebViewDelegate>
@property (nonatomic , weak) IBOutlet UIWebView *webView;

@end
