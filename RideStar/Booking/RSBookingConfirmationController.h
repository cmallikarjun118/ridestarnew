//
//  RSBookingConfirmationController.h
//  RideStar
//
//  Created by Local user on 12/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RSSharedViewController.h"

@interface RSBookingConfirmationController : RSSharedViewController<UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate>
@property (nonatomic , weak) IBOutlet UITableView *tableView;
@property(nonatomic, strong) NSDictionary *cabDetails;
@end
