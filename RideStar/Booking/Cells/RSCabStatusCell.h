//
//  RSCabStatusCell.h
//  RideStar
//
//  Created by Local user on 16/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSCabStatusCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *cabStatusButton;
@property (weak, nonatomic) IBOutlet UIView *lineView;

-(void)setImageForCabStatus:(NSDictionary *)cabStatsuDict;


@end
