//
//  RSOffersCollectionViewCell.h
//  RideStar
//
//  Created by Local user on 27/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSOffersCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *offerImageView;
@property (weak, nonatomic) IBOutlet UIButton *shopButton;
@property (weak, nonatomic) IBOutlet UIButton *laterButton;

-(void)setData:(NSDictionary *)dict;
@end
