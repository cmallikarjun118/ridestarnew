//
//  RSDriverDetailsCell.m
//  RideStar
//
//  Created by Local user on 27/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSDriverDetailsCell.h"



@implementation RSDriverDetailsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)callButtonAction:(id)sender {
    
}

-(void)setCabDetails:(NSDictionary *)details{
    self.driverName.text = [details valueForKey:@"driver_name"];
    self.vehicleNumber.text = [details valueForKey:@"vehicle_number"];
    self.cabName.text = [details valueForKey:@"vehicle_model"];
}

@end
