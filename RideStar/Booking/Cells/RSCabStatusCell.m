//
//  RSCabStatusCell.m
//  RideStar
//
//  Created by Local user on 16/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSCabStatusCell.h"

@implementation RSCabStatusCell

- (void)awakeFromNib {
    [super awakeFromNib];
//    self.cabStatusButton.layer.cornerRadius = self.cabStatusButton.frame.size.height/2;
//    self.cabStatusButton.clipsToBounds = YES;
    
    // Initialization code
}
-(void)setImageForCabStatus:(NSDictionary *)cabStatsuDict {
    NSString *rowTittle = [cabStatsuDict objectForKey:@"Name"];
    self.nameLabel.text = rowTittle;
    BOOL isSelected  = [[cabStatsuDict objectForKey:@"isSelected"] boolValue];
    if ([rowTittle isEqualToString:@"Your Booking is Accepted"]) {
        [self.cabStatusButton setBackgroundImage:[UIImage imageNamed:isSelected ? @"BookAccepted":@"BookPending"] forState:UIControlStateNormal];
    }else if ([rowTittle isEqualToString:@"Cab Started"]){
        [self.cabStatusButton setBackgroundImage:[UIImage imageNamed:isSelected ? @"CabStarted":@"CabPending"] forState:UIControlStateNormal];
    }else if ([rowTittle isEqualToString:@"Cab Arrived"]){
        [self.cabStatusButton setBackgroundImage:[UIImage imageNamed:isSelected ? @"Cabarrived":@"CabPending"] forState:UIControlStateNormal];
    }else if ([rowTittle isEqualToString:@"Ride Started"]){
        [self.cabStatusButton setBackgroundImage:[UIImage imageNamed:isSelected ? @"RideStarted":@"CabPending"] forState:UIControlStateNormal];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
