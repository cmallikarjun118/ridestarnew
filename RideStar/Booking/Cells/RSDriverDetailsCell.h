//
//  RSDriverDetailsCell.h
//  RideStar
//
//  Created by Local user on 27/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSDriverDetailsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cabName;
@property (weak, nonatomic) IBOutlet UILabel *vehicleNumber;
@property (weak, nonatomic) IBOutlet UILabel *driverName;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
- (IBAction)callButtonAction:(id)sender;



-(void)setCabDetails:(NSDictionary *)details;
@end