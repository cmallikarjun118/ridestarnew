//
//  RSOffersCell.m
//  RideStar
//
//  Created by Local user on 16/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSOffersCell.h"


#import "RSOffersCollectionViewCell.h"

#import "UIColor+Zigy.h"
#import "RSServiceInteractor.h"

#import <SDWebImage/UIImageView+WebCache.h>

@implementation RSOffersCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.offersArray  = [NSMutableArray array];
    [self getOffersDetails];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"RSOffersCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"RSOffersCollectionViewCell"];
    
    // Initialization code
}

#pragma mark - UICollectionViewDatasource

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(self.collectionView.frame.size.width,self.collectionView.frame.size.height-10);
    //    return CGSizeMake(self.collectionView.frame.size.width, self.view.frame.size.height-(self.bookingConfirmationView.frame.size.height+self.pageController.frame.size.height+60));
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.offersArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RSOffersCollectionViewCell  *offersCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RSOffersCollectionViewCell" forIndexPath:indexPath];
    offersCell.contentView.layer.borderColor=[UIColor RSOrangeColour].CGColor ;
    offersCell.contentView.layer.borderWidth=2.0;
    offersCell.contentView.layer.cornerRadius=10.0;
    offersCell.contentView.clipsToBounds=YES;
    
    
    self.dict=[self.offersArray objectAtIndex:indexPath.row];
    
    [offersCell.offerImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[self.dict objectForKey:@"image"]]] ];
    [offersCell.shopButton addTarget:self action:@selector(shopButtonAction) forControlEvents:UIControlEventTouchDown];
  
    
    return offersCell;
}
#pragma mark - UICollectionViewDataSource


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma API Call

-(void)getOffersDetails{
    NSString *url = @"city=bangalore";
    [RSServiceInteractor GetDeals:url withCompetion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        self.offersArray = [dataDict mutableCopy];
        NSLog(@"deals array is%@",self.offersArray);
        [self.collectionView reloadData];
    }];
    
}

#pragma button Actions
-(void)shopButtonAction{
    NSURL *url = [NSURL URLWithString:[self.dict objectForKey:@"link"]];
    NSLog(@"%@",url);
    if ([[UIApplication sharedApplication]canOpenURL:url]){
        [[UIApplication sharedApplication] openURL:url];
    }
    else
    {
        [self showAlertMessage:@"" withMessage:@"Please Try Again"];
        
    }

    
}

#pragma Helper Methods
-(void)showAlertMessage:(NSString *)tittle withMessage:(NSString *)message
{
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:tittle message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
    
}


@end
