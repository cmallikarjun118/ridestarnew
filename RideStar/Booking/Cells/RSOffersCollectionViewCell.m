//
//  RSOffersCollectionViewCell.m
//  RideStar
//
//  Created by Local user on 27/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSOffersCollectionViewCell.h"

#import <SDWebImage/UIImageView+WebCache.h>

@implementation RSOffersCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setData:(NSDictionary *)dict{
     [self.offerImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dict objectForKey:@"image"]]] ];
}

@end
