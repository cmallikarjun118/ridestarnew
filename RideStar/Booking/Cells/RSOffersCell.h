//
//  RSOffersCell.h
//  RideStar
//
//  Created by Local user on 16/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSOffersCell : UITableViewCell<UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic , weak) IBOutlet UICollectionView *collectionView;

@property (nonatomic , strong) NSMutableArray *offersArray;
@property(nonatomic, strong)NSDictionary *dict;


@end
