//
//  RSWebViewController.m
//  RideStar
//
//  Created by Local user on 17/07/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSWebViewController.h"
#import "RSBookingConfirmationController.h"

#import "ZMConstant.h"
#import "RSHomeModel.h"
#import "RSLoginModel.h"
#import "RSServiceInteractor.h"

#import "RNActivityView.h"
#import "UIView+RNActivityView.h"


@interface RSWebViewController (){
    BOOL isTokenTaken;
}

@end

@implementation RSWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([[RSHomeModel sharedInstance].cabType isEqualToString:@"OLA"]) {
      [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:olaWebUrl]]];
        self.title = @"OLA Login";
    }else{
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:uberWebUrl]]];
        self.title = @"UBER Login";
    }
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    isTokenTaken = NO;
}

#pragma mark  - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [self.view showCustomActivityView];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.view hideActivityView];
    NSURL *Currenturl =[NSURL URLWithString:webView.request.URL.absoluteString];
    
    if ([[RSHomeModel sharedInstance].cabType isEqualToString:@"OLA"]) {
        NSArray *token = [Currenturl.fragment componentsSeparatedByString:@"="];
        NSLog(@"array:%@",token);
        NSArray *tokenvalue = [[token objectAtIndex:1] componentsSeparatedByString:@"&state"];
        NSString *tokenString =[tokenvalue objectAtIndex:0];
        if (tokenString.length>0) {
            [self tokenAdd:tokenString];
             NSLog(@"token value:%@",tokenString);
        }else{
            NSLog(@"invalid token");
        }
    }else{
        NSArray *token = [Currenturl.query componentsSeparatedByString:@"&"];
        NSArray *tokenarray=[[token objectAtIndex:1] componentsSeparatedByString:@"state="];
        NSString *bearerToken = [tokenarray objectAtIndex:1];
        if (bearerToken.length>10) {
            [self tokenAdd:bearerToken];
            NSLog(@"token value:%@",bearerToken);
        }else{
            NSLog(@"invalid token");
        }
        
    }

}
/*
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.view hideActivityView];
    NSString *currentURL = webView.request.URL.absoluteString;
    if ([[RSHomeModel sharedInstance].cabType isEqualToString:@"OLA"]) {
        currentURL = [currentURL stringByReplacingOccurrencesOfString:@"#" withString:@"&"];
    }else{
        currentURL = [currentURL stringByReplacingOccurrencesOfString:@"?" withString:@"&"];
    }
    NSURL * url =[NSURL URLWithString:currentURL];
    NSLog(@"url recieved: %@", url);
    NSLog(@"query string: %@", [url query]);
    NSLog(@"host: %@", [url host]);
    NSLog(@"url path: %@", [url path]);
  //  •	http://52.74.142.219//rideStarApp/?methodName=bearerToken.update&user_id=12&service_name=OLA&token_value=787613cc33dc434dac6af2f787644218
    NSString *token;
    if (!isTokenTaken) {
        if (![[RSHomeModel sharedInstance].cabType isEqualToString:@"OLA"]) {
            NSArray *pairs = [currentURL componentsSeparatedByString:@"&"];
            if (pairs.count > 2) {
                NSString * tokenString = [pairs objectAtIndex:1];
                NSArray *elements = [tokenString componentsSeparatedByString:@"="];
                token = [elements objectAtIndex:1];
                if (token.length>0) {
                    [self tokenAdd:token];
                    isTokenTaken=YES;
                }
            }
            
        }else{
            NSDictionary *dict = [self parseQueryString:currentURL];
            token = [dict objectForKey:@"state"];
            if (token.length>0) {
                [self tokenAdd:token];
                isTokenTaken=YES;
                
            }
        }

    }
}
- (NSDictionary *)parseQueryString:(NSString *)query {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:6];
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    for (NSString *pair in pairs) {
        NSArray *elements = [pair componentsSeparatedByString:@"="];
        NSString *key = [[elements objectAtIndex:0] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *val = [[elements objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [dict setObject:val forKey:key];
    }
    return dict;
}
 */
- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error{
    
}

-(void)callBookingApi{
    http://54.169.50.112/rideStarApp/?methodName=book.cab&user_id=1&service_name=OLA&product_id=mini&pickup_lat=13.003022&pickup_lng=77.660978&drop_lat=12.957156&drop_lng=77.700623
    [self.view showCustomActivityView];
    NSString *urlString = [NSString stringWithFormat:@"user_id=%@&service_name=%@&product_id=%@&pickup_lat=%@&pickup_lng=%@&drop_lat=%@&drop_lng=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userId"],[RSHomeModel sharedInstance].cabType,[RSHomeModel sharedInstance].cabId,[RSHomeModel sharedInstance].sourceLattitue,[RSHomeModel sharedInstance].sourceLongitude,[RSHomeModel sharedInstance].destinationLattitude,[RSHomeModel sharedInstance].destinationLogitude];
    [RSServiceInteractor bookCab:urlString withCompletion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (success) {
            RSBookingConfirmationController *cabDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"RSBookingConfirmationController"];
            [RSHomeModel sharedInstance].bookedCabDetails = dataDict;
            [self.navigationController pushViewController:cabDetails animated:YES];
        }
    }];
    

}

-(void)tokenAdd:(NSString *)tokenString{
    //52.74.142.219//rideStarApp/?methodName=bearerToken.add&user_id=12&service_name=OLA&token_value=787613cc33dc434dac6af2f787644218
    NSString *addTokenUrl = [NSString stringWithFormat:@"user_id=%@&service_name=%@&token_value=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"userId" ],[RSHomeModel sharedInstance].cabType,tokenString];
    
    [RSServiceInteractor addBearerToken:addTokenUrl withCompletion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if ([dataDict valueForKey:@"user_token_id"]>0) {
             NSLog(@"token add:%@",dataDict);
            [self callBookingApi];
        }
       
    }];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
