//
//  RSCabDetailsCell.m
//  RideStar
//
//  Created by sanjay on 22/06/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSCabDetailsCell.h"

@implementation RSCabDetailsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.bookNowButton.layer.cornerRadius = 5;
    self.bookNowButton.clipsToBounds = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setData:(NSDictionary *)details{
    self.serviceProviderLabel.text =[NSString stringWithFormat:@"%@", [details objectForKey:@"service_name"]];
    self.cabNameLabel.text =[NSString stringWithFormat:@"%@",[details objectForKey:@"display_name"]];
    self.cashBackLabel.text = [NSString stringWithFormat:@"Cashback %@",[details objectForKey:@"cashback_amount"]];
    self.costLabel.text = [NSString stringWithFormat:@"%@",[details objectForKey:@"estimated_amount"]];
    self.detailsLabel.text =[NSString stringWithFormat:@"Min ₹%@|₹%@-Km|%@ min|Surge %@",[details objectForKey:@"base_fare"],[details objectForKey:@"cost_per_distance"],[details objectForKey:@"ride_cost_per_minute"],[details objectForKey:@"surcharge"]];
    self.distanceLabel.text=[NSString stringWithFormat:@"%@ Min ",[details objectForKey:@"eta"]];
}

@end
