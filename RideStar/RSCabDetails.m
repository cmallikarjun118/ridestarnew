//
//  RSCabDetails.m
//  RideStar
//
//  Created by sanjay on 22/06/16.
//  Copyright © 2016 Byssotech. All rights reserved.
//

#import "RSCabDetails.h"
#import "RSWebViewController.h"
#import "RSBookingConfirmationController.h"

#import "RSCabDetailsCell.h"
#import "RSLoginModel.h"


#import "RSHomeModel.h"
#import "RSServiceInteractor.h"

#import "TSMessage.h"
#import "TSMessageView.h"

#import "UIColor+Zigy.h"
#import "RNActivityView.h"
#import "UIImage+Color.h"
#import "UIScrollView+EmptyDataSet.h"
#import "UIView+RNActivityView.h"


@interface RSCabDetails ()<DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@end

@implementation RSCabDetails

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"AVilable Rides";
    [self.cabButton setImage:[UIImage imageNamed:@"CabWhite"] forState:UIControlStateNormal];
    [self.shareButton setImage:[UIImage imageNamed:@"Sharewhite"] forState:UIControlStateNormal];
    [self.autoButton setImage:[UIImage imageNamed:@"AutoWhite"] forState:UIControlStateNormal];
    [self.shuttlButton setImage:[UIImage imageNamed:@"ShuttleWhite"] forState:UIControlStateNormal];
    
    [self.cabButton setImage:[UIImage imageNamed:@"CabSelect"] forState:UIControlStateSelected];
    [self.shareButton setImage:[UIImage imageNamed:@"ShareSelect"] forState:UIControlStateSelected];
    [self.autoButton setImage:[UIImage imageNamed:@"AutoSelect"] forState:UIControlStateSelected];
    [self.shuttlButton setImage:[UIImage imageNamed:@"ShuttleSelect"] forState:UIControlStateSelected];
    self.cabButton.selected = YES;
    self.selectedType = cabSelected;
    self.headerView.frame =  CGRectMake(0, 0, self.tableView.frame.size.width, 60);
    self.tableView.tableHeaderView = self.headerView;
    self.cabsList = [NSArray array];
    [self getCabsList];
    
    
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    // A little trick for removing the cell separators
    self.tableView.tableFooterView = [UIView new];
    //  self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]];
}

-(void)getCabsList{
    
   // http://54.169.50.112/rideStarApp/?methodName=get.cabsData&pickup_lat=12.990654&pickup_lng=77.7306318&drop_lat=12.9709692&drop_lng=77.714324&mode=NOW&more_status=1
    NSString *urlString = [NSString stringWithFormat:@"pickup_lat=%@&pickup_lng=%@&drop_lat=%@&drop_lng=%@",[RSHomeModel sharedInstance].sourceLattitue,[RSHomeModel sharedInstance].sourceLongitude,[RSHomeModel sharedInstance].destinationLattitude,[RSHomeModel sharedInstance].destinationLogitude];
    NSLog(@"URL For cabdetails:%@",urlString);
    [RSServiceInteractor getCabs:urlString withCompletion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if(success){
            [[RSHomeModel sharedInstance].autoArray removeAllObjects];
            [[RSHomeModel sharedInstance].shuttleArray removeAllObjects];
            [[RSHomeModel sharedInstance].cabsArray removeAllObjects];
            [[RSHomeModel sharedInstance].sharesArray removeAllObjects];
            NSArray *array = [dataDict objectForKey:@"categories"];
            NSDictionary *dict = [array objectAtIndex:0];
            [RSHomeModel sharedInstance].autoArray = [dict valueForKeyPath:@"auto"];
            [RSHomeModel sharedInstance].shuttleArray = [dict valueForKeyPath:@"shuttle"];
            [RSHomeModel sharedInstance].cabsArray = [dict valueForKeyPath:@"taxi"];
            [RSHomeModel sharedInstance].sharesArray = [dict valueForKeyPath:@"other"];
            [self.tableView reloadData];
        }
    }];

}
#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 110;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.selectedType == shuttleSelected) {
        return [RSHomeModel sharedInstance].shuttleArray.count;
    }else  if (self.selectedType == autoSelected) {
        return [RSHomeModel sharedInstance].autoArray.count;
    }else  if (self.selectedType == shareSelected) {
        return [RSHomeModel sharedInstance].sharesArray.count;
    }
    return [RSHomeModel sharedInstance].cabsArray.count;

}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier=@"RSCabDetailsCell";
    RSCabDetailsCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [cell.bookNowButton addTarget:self action:@selector(bookNowButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setData:[self getDictionary:indexPath.row]];
    return cell;
}
#pragma mark - ActionMethods

-(IBAction)bookNowButtonAction:(UIButton *)sender{
    [RSHomeModel sharedInstance].cabType =[[self getDictionary:sender.tag]valueForKey:@"service_name"];
    [RSHomeModel sharedInstance].cabId = [[self getDictionary:sender.tag] valueForKey:@"id"];
    NSLog(@"%@",[RSHomeModel sharedInstance].cabType);
    if ([[RSHomeModel sharedInstance].cabType isEqualToString:@"BMTC"] || [[RSHomeModel sharedInstance].cabType isEqualToString:@"METRO"]){
        RSBookingConfirmationController *cabDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"RSBookingConfirmationController"];
        [self.navigationController pushViewController:cabDetails animated:YES];
        
    }else{
    [self checkForToken];
    }
}

-(void)checkForToken{
    [self.view showCustomActivityView];
    NSString  *urlString = [NSString stringWithFormat:@"user_id=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userId"]];
    [RSServiceInteractor checkForToken:urlString withCompletion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        [self.view hideActivityView];
        if (success) {
            [RSLoginModel sharedInstance].isOlaToken = [[dataDict objectForKey:@"ola_token_status"] boolValue];
            [RSLoginModel sharedInstance].isUbertoken = [[dataDict objectForKey:@"uber_token_status"] boolValue];
            if ([[RSHomeModel sharedInstance].cabType isEqualToString:@"OLA"]&&![RSLoginModel sharedInstance].isOlaToken) {
                RSWebViewController    *detailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RSWebViewController"];
                [self.navigationController pushViewController:detailsVC animated:YES];
            }
            else if ([[RSHomeModel sharedInstance].cabType isEqualToString:@"UBER"]&& ![RSLoginModel sharedInstance].isUbertoken){
                RSWebViewController    *detailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RSWebViewController"];
                [self.navigationController pushViewController:detailsVC animated:YES];
                
            }
            else{
                [self bookNowAPI];
            }

        }else{
            [self showErrorAlert:[dataDict objectForKey:@"responseInfo"]];
        }
    }];
}

- (IBAction)cabsButtonAction:(UIButton *)sender {
    self.selectedType = cabSelected;
    for (UIView *view in self.cabTypesHolderView.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)view;
            if (sender.tag == view.tag) {
                button.selected = YES;
            }else{
                button.selected = NO;
            }
        }
    }
    [self.tableView reloadData];
}

- (IBAction)shareButtonAction:(UIButton *)sender {
    self.selectedType = shareSelected;
    for (UIView *view in self.cabTypesHolderView.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)view;
            if (sender.tag == view.tag) {
                button.selected = YES;
            }else{
                button.selected = NO;
            }
        }
    }
    [self.tableView reloadData];
}

- (IBAction)autoButtonAction:(UIButton *)sender {
    self.selectedType = autoSelected;
    for (UIView *view in self.cabTypesHolderView.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)view;
            if (sender.tag == view.tag) {
                button.selected = YES;
            }else{
                button.selected = NO;
            }
        }
    }
    [self.tableView reloadData];
}

- (IBAction)shuttleAction:(UIButton *)sender {
    self.selectedType =   shuttleSelected;
    for (UIView *view in self.cabTypesHolderView.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)view;
            if (sender.tag == view.tag) {
                button.selected = YES;
            }else{
                button.selected = NO;
            }
        }
    }
    [self.tableView reloadData];
}



#pragma mark - Alert

-(void)showSuccessAlert:(NSString *)tittle{
    [TSMessage showNotificationInViewController:self.presentingViewController title:NSLocalizedString(@"Success", nil) subtitle:tittle type:TSMessageNotificationTypeSuccess];
    
}

-(void)showErrorAlert:(NSString *)tittle{
    [self.navigationController.view hideActivityView];
    [TSMessage showNotificationInViewController:self.parentViewController title:NSLocalizedString(@"Failure", nil) subtitle:tittle type:TSMessageNotificationTypeError];
}

#pragma mark - Healper 

-(NSDictionary *)getDictionary:(NSInteger)index{
    
    NSDictionary *dict;
    if (self.selectedType==cabSelected) {
    dict=[[RSHomeModel sharedInstance].cabsArray objectAtIndex:index];
    }
    else if (self.selectedType==shareSelected){
       dict=[[RSHomeModel sharedInstance].sharesArray objectAtIndex:index];
    }
    else if (self.selectedType==autoSelected){
        dict=[[RSHomeModel sharedInstance].autoArray objectAtIndex:index];
    }
    else if (self.selectedType==shuttleSelected){
        dict=[[RSHomeModel sharedInstance].shuttleArray objectAtIndex:index];
    }

    return dict;
}


-(NSIndexPath*)indexPathForView:(UIView*)view{
    while (view && ![view isKindOfClass:[UITableViewCell class]])
        view = view.superview;
    return [self.tableView indexPathForRowAtPoint:view.center];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)bookNowAPI{
    [self.view showCustomActivityView];
    NSString *urlString = [NSString stringWithFormat:@"user_id=%@&service_name=%@&product_id=%@&pickup_lat=%@&pickup_lng=%@&drop_lat=%@&drop_lng=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userId"],[RSHomeModel sharedInstance].cabType,[RSHomeModel sharedInstance].cabId,[RSHomeModel sharedInstance].sourceLattitue,[RSHomeModel sharedInstance].sourceLongitude,[RSHomeModel sharedInstance].destinationLattitude,[RSHomeModel sharedInstance].destinationLogitude];
    [RSServiceInteractor bookCab:urlString withCompletion:^(BOOL success, NSString *message, NSDictionary *dataDict, NSInteger code) {
        if (success) {
            RSBookingConfirmationController *cabDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"RSBookingConfirmationController"];
            [RSHomeModel sharedInstance].bookedCabDetails = dataDict;
            [self.navigationController pushViewController:cabDetails animated:YES];
        }
    }];
}


#pragma mark - empty DataSource

-(BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView{
    return YES;
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView{
    return [UIImage imageNamed:@"CabStarted"];
}
- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"No Rides Avilable Please select another option";
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:12],
                                 NSForegroundColorAttributeName: [UIColor blackColor],
                                 NSParagraphStyleAttributeName: paragraph};
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView{
    return 50;
}
- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView{
    return -50;
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state
{
    NSString *text = @"CHANGE LOCATION";
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:12],
                                 NSForegroundColorAttributeName: [UIColor whiteColor ],
                                 NSParagraphStyleAttributeName: paragraph};
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}


-(UIImage*)buttonBackgroundImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state{
    return [UIImage imageWithColor:[UIColor RSOrangeColour] size:CGSizeMake(1, 35)];
}


- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
